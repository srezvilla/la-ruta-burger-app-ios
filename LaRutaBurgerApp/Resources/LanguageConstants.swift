//
//  LanguageConstants.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

// MARK - Language constants

public enum LanguageConstant: String  {
    
    // App languages
    case key_language                                   = "key_language"
    case key_languange_EN                               = "key_languange_EN"
    case key_languange_ES                               = "key_languange_ES"

    // Common
    case key_mainTitle                                  = "key_mainTitle"
    case key_cancel                                     = "key_cancel"
    case key_email                                      = "key_email"
    case key_fullName                                   = "key_fullName"
    case key_password                                   = "key_password"
    case key_repeatPassword                             = "key_repeatPassword"
    case key_next                                       = "key_next"
    case key_done                                       = "key_done"
    case key_ok                                         = "key_ok"
    case key_error                                      = "key_error"
    case key_send                                       = "key_send"
    case key_notAvailable                               = "key_notAvailable"
    case key_title                                      = "key_title"
    case key_warning                                    = "key_warning"
    case key_search                                     = "key_search"
    case key_noResults                                  = "key_noResults"
    
    // Login
    case key_forgotPassword                             = "key_forgotPassword"
    case key_login                                      = "key_login"
    case key_newUser                                    = "key_newUser"
    case key_anonymousUser                              = "key_anonymousUser"
    case key_anonymousUserText                          = "key_anonymousUserText"
    case key_loginWarningText                           = "key_loginWarningText"
    case key_newUserConfirmation                        = "key_newUserConfirmation"
    case key_anonymousWarning                           = "key_anonymousWarning"
    
    // Reset Password
    case key_resetPasswordTitle                         = "key_resetPasswordTitle"
    case key_resetPasswordText                          = "key_resetPasswordText"
    case key_resetPasswordConfirmation                  = "key_resetPasswordConfirmation"
    
    // New user
    case key_registerWarningText                        = "key_registerWarningText"
    case key_registerWarningText2                       = "key_registerWarningText2"
    
    // Tab titles
    case key_topRanking                                 = "key_topRanking"
    case key_burgers                                    = "key_burgers"
    case key_restaurants                                = "key_restaurants"
    case key_burgerMap                                  = "key_burgerMap"
    
    // Menu
    case key_logOut                                     = "key_logOut"
    case key_languageSpanish                            = "key_languageSpanish"
    case key_languageEnglish                            = "key_languageEnglish"
    
    // Restaurants
    case key_reviewTitle                                = "key_reviewTitle"
    case key_reviewNoResults                            = "key_reviewNoResults"
    
    // Bugers
    case key_newBurger                                  = "key_newBurger"
    case key_restaurant                                 = "key_restaurant"
    case key_camera                                     = "key_camera"
    case key_gallery                                    = "key_gallery"

}
