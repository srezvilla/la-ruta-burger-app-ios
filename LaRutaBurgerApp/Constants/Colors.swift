//
//  Colors.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit


// MARK - Color constants

public struct CustomColor {
    
    // Main colors
    
    public static let mainBackGround = UIColor(red: 73, green: 79, blue: 92)
    public static let lightBackGround = UIColor(red: 161, green: 167, blue: 178)
    public static let burgerRed = UIColor(red: 206, green: 10, blue: 36)
    public static let mainBackGroundAlpha30 = UIColor(red: 73, green: 79, blue: 92, alphaOpacity: 0.3)
    public static let boxShadow45 = UIColor(red: 0, green: 0, blue: 0, alphaOpacity: 0.45)
    public static let tangerine = UIColor(red: 255, green: 144, blue: 23)
    public static let tangerineAlpha = UIColor(red: 255, green: 144, blue: 23, alphaOpacity: 0.1)
    
    // Base colors
    public static let white = UIColor.white
    public static let black = UIColor.black
    public static let blue = UIColor.blue
    public static let gray = UIColor.gray
    public static let lightGray = UIColor.lightGray
    
    // Blue colors
    public static let cerulean = UIColor(red: 0, green: 151, blue: 196)
    
    // Gray colors
    public static let greyishBrownThree = UIColor(red: 87, green: 79, blue: 74)
    
    // Colors with alpha
    public static let blackAlpha50 = UIColor(red: 0, green: 0, blue: 0, alphaOpacity: 0.50)
    public static let blackAlpha80 = UIColor(red: 0, green: 0, blue: 0, alphaOpacity: 0.8)
    public static let blackAlpha20 = UIColor(red: 0, green: 0, blue: 0, alphaOpacity: 0.2)
    public static let blackAlpha10 = UIColor(red: 0, green: 0, blue: 0, alphaOpacity: 0.1)
}
