//
//  AppConstants.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit
import MapKit


// MARK: - App constants

let AppSession: AppSessionManager = AppSessionManager.sharedInstance
let dbManager: DbManager = DbManager.sharedInstance
let storegaeManager: StorageManager = StorageManager.sharedInstance
let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate

struct AppConstant {
    
    static let defaultLanguage: AppLanguage = .en
    static let navBarItemSize: CGFloat = 28
    static let spaceBetweenKeyboardAndTextField: CGFloat = 10

}

// MARK: - Map constants

let defaultUserLocation = CLLocation(latitude: 40.4381311, longitude: -3.8196221)

// MARK: - System constants

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height


// MARK: - Anonymous login constants

let anonymousEmail = "larutaanonimo@email.com"
let anonymousPassword = "123456"
