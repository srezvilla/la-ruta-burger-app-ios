//
//  Images.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

public struct CustomImage {
    
    struct NavBar {
        
        public static let burger = #imageLiteral(resourceName: "BurgerIconImage")
        public static let menu = #imageLiteral(resourceName: "menuIcon")
        public static let back = #imageLiteral(resourceName: "backIcon")
        public static let editIcon = #imageLiteral(resourceName: "editIcon")
        public static let plusIcon = #imageLiteral(resourceName: "plusSolid")
        public static let searchIcon = #imageLiteral(resourceName: "searchSolid")
        public static let searchIconOrange = #imageLiteral(resourceName: "searchOrange")
    }
    
    struct Icon {
        
        public static let key = #imageLiteral(resourceName: "keyIcon")
        public static let logOut = #imageLiteral(resourceName: "exitIcon")
        public static let circle = #imageLiteral(resourceName: "circleIcon")
        public static let circleFill = #imageLiteral(resourceName: "circleIconOrange")
        public static let userLocation = #imageLiteral(resourceName: "userLocation")
        public static let rightButtonLocation = #imageLiteral(resourceName: "rightButton")
        public static let rightIcon = #imageLiteral(resourceName: "rightIcon")
        public static let likeEmpty = #imageLiteral(resourceName: "heartEmpty")
        public static let like = #imageLiteral(resourceName: "heartSolid")
    }
    
    struct Img {
        
        // Map marketd
        
        public static let burgerMarker = #imageLiteral(resourceName: "burgerMarker")
        public static let goldMarker = #imageLiteral(resourceName: "goldMarker")
        public static let silverMarker = #imageLiteral(resourceName: "silverMarker")
        public static let bronzeMarker = #imageLiteral(resourceName: "bronzeMarker")
        
        //Stars
        public static let star = #imageLiteral(resourceName: "star")
        public static let halfStar = #imageLiteral(resourceName: "halfStar")
        public static let emptyStar = #imageLiteral(resourceName: "emptyStar")
        public static let starBig = #imageLiteral(resourceName: "starBig")
        public static let halfStarBig = #imageLiteral(resourceName: "starHalfBig")
        public static let emptyStarBig = #imageLiteral(resourceName: "starEmptyBig")
        
        // Burgers
        public static let likeBurger = #imageLiteral(resourceName: "likeBurger")
        public static let likeBurgerEmpty = #imageLiteral(resourceName: "likeBurgerEmpty")
        public static let restauranBurger = #imageLiteral(resourceName: "restaurantBurger")
        
        // Ranking
        public static let goldAward = #imageLiteral(resourceName: "goldAward")
        public static let silverAward = #imageLiteral(resourceName: "silverAward")
        public static let bronzeAward = #imageLiteral(resourceName: "bronzeAward")
        
    }
}
