//
//  AppEnumerations.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

// MARK: - Firebase collections

public enum collections: String {
    
    case users = "users"
    case restaurants = "restaurants"
    case burgers = "burgers"
    case reviews = "reviews"
    
}

// MARK: - Storyboard

public enum StoryBoard: String {
    
    case ranking = "Ranking"
    case burgers = "Burgers"
    case restaurants = "Restaurants"
    case map = "Map"
}


// MARK: - App language

public enum AppLanguage: String, Localized {
    
    case en = "EN"
    case es = "ES"
    
    var localized: String {
        
        switch self {
        case .en: return localizable(.key_languange_EN)
        case .es: return localizable(.key_languange_ES)
        }
    }
    
    var localizedFile: String {
        
        switch self {
        case .en: return "en"
        case .es: return "es"
        }
    }
}
