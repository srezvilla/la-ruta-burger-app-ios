//
//  Fonts.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit


// MARK - Font constants

public struct CustomFont {
    
    
    // MARK: - SFProText
    
    // PRO text regular
    public static let regular12 = UIFont(name: "SFProText-Regular", size: 12)!
    public static let regular8 = UIFont(name: "SFProText-Regular", size: 8)!
    
    // PRO text semibold
    public static let semibold14 = UIFont(name: "SFProText-Semibold", size: 14)!
}
