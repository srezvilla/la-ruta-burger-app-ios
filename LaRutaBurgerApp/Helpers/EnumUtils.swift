//
//  EnumUtils.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class EnumUtils {
    
    class func indexOfType(_ type: AnyHashable?, inArray array: [AnyHashable]?) -> Int? {
        
        if let type = type {
            
            return array?.index(of: type)
        }
        
        return nil
    }
}


// MARK: - Iterable

protocol Iterable {}

extension RawRepresentable where Self: RawRepresentable {
    
    static func iterateEnum<T: Hashable>(_: T.Type) -> AnyIterator<T> {
        
        var i = 0
        
        return AnyIterator {
            
            let next = withUnsafePointer(to: &i) {
                
                $0.withMemoryRebound(to: T.self, capacity: 1) { $0.pointee }
            }
            
            if next.hashValue != i { return nil }
            
            i += 1
            
            return next
        }
    }
}

extension Iterable where Self: RawRepresentable, Self: Hashable {
    
    static func hashValues() -> AnyIterator<Self> {
        
        return iterateEnum(self)
    }
    
    static func rawValues() -> [Self.RawValue] {
        
        return hashValues().map({$0.rawValue})
    }
}


// MARK: - Localized

protocol Localized: Iterable {
    
    var localized: String { get }
}

extension Localized where Self: RawRepresentable, Self: Hashable {
    
    static func hashAtIndex(_ index: Int) -> Self? {
        
        return hashValues().map({$0})[index]
    }
    
    static func localizedRawValues() -> [String] {
        
        var localizedItemsArray: [String] = []
        
        for enumItem in hashValues() {
            
            localizedItemsArray.append(enumItem.localized)
        }
        
        return localizedItemsArray
    }
}

protocol CaseCountable {
    
    static var caseCount: Int { get }
}

extension CaseCountable where Self: RawRepresentable, Self.RawValue == Int {
    
    internal static var caseCount: Int {
        
        var count = 0
        
        while let _ = Self(rawValue: count) {
            
            count += 1
        }
        
        return count
    }
}

extension Array where Element: Localized {
    
    func localizedValues() -> [String] {
        
        var localizedItemsArray: [String] = []
        
        for enumItem in self {
            
            localizedItemsArray.append(enumItem.localized)
        }
        
        return localizedItemsArray
    }
}
