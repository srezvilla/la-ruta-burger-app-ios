//
//  StringUtils.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class StringUtils {
    
    
    // Mark - Public
    
    public static func format(_ format: String, _ args: [CVarArg]) -> String {
        
        return withVaList(args) {
            
            formatter()(format, $0)
        }
    }
    
    
    // Mark - Private
    
    private static func formatter() -> (_ format: String, _ args: CVaListPointer) -> String {
        
        return { (format: String, args: CVaListPointer) in
            
            return NSString(format: format, arguments: args) as String
        }
    }
}


// MARK - Localizable

public func localizable(_ key: LanguageConstant) -> String {
    
    let bundle = AppUtils.getStringsResourceBundle()
    
    return NSLocalizedString(key.rawValue, tableName: nil, bundle: bundle, value: "", comment: "")
}

public func localizable(_ key: LanguageConstant, _ args: CVarArg...) -> String {
    
    let translatedString = localizable(key)
    
    return StringUtils.format(translatedString, args)
}
