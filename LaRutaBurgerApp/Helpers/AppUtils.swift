//
//  AppUtils.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class AppUtils {
    
    // MARK: - Keyboard related methods
    
    class func keyboardHeightFromNotification(_ notification: NSNotification) -> CGFloat {
        
        guard let userInfo = notification.userInfo,
            let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect else {
                
                return 0
        }
        
        return keyboardFrame.height + AppConstant.spaceBetweenKeyboardAndTextField
    }
    
    
    class func dismissKeyboard() {
        
        appDelegate.window?.endEditing(true)
    }
 
    // MARK: - Localizable strings related
    
    class func getStringsResourceBundle() -> Bundle {
        
        let appLanguage = AppSession.appPreferredLanguage.localizedFile
        let path = Bundle.main.path(forResource: appLanguage, ofType: "lproj")
        return Bundle(path: path!)!
    }
    
    
}
