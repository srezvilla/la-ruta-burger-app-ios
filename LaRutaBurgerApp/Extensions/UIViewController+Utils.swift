//
//  UIViewController+Utils.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // MARK: - Properties
    private static let tapRecognizerAssociation = ObjectAssociation<UITapGestureRecognizer>()
    
    var tapRecognizer: UITapGestureRecognizer? {
        
        get { return UIViewController.tapRecognizerAssociation[self] }
        set { UIViewController.tapRecognizerAssociation[self] = newValue }
    }
    
    // MARK: - Loader HUD
    
    func showLoader() {
        
        LoaderHudView.show(inViewController : self)
    }
    
    func hideLoader() {
        
        LoaderHudView.hide(inViewController: self)
    }
    
    
    // MARK: - Files
    
    func shareItems(_ items: [Any]?) {
        
        if let items = items {
            
            let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = view
            
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - Navigation
    
    func gotoMenu() {
        
        let view = MenuViewController()
        
        navigationController?.pushViewController(view, animated: true)
    }
    
    func pushViewController(_ viewController: UIViewController, animated: Bool? = true) {
        
        navigationController?.pushViewController(viewController, animated: animated!)
    }
    
    func popToRoot(animated: Bool? = true) {
        
        navigationController?.popToRootViewController(animated: animated!)
    }
    
    func popViewController(animated: Bool? = true) {
        
        navigationController?.popViewController(animated: animated!)
    }
    
    func popToViewController(animated: Bool = true, numberOfScreensBack: Int) {
        
        if let navigationController = navigationController,
            navigationController.viewControllers.count > numberOfScreensBack {
            
            navigationController.popToViewController(navigationController.viewControllers[navigationController.viewControllers.count - numberOfScreensBack], animated: animated)
            
        } else {
            
            popToRoot(animated: animated)
        }
    }
    
    func dismiss(animated: Bool? = true) {
        
        dismiss(animated: animated!, completion: nil)
    }
    
    
    // MARK: - Tab bar controller
    
    func showTabBar() {
        
        tabBarController?.tabBar.isHidden = false
    }
    
    func hideTabBar() {
        
        tabBarController?.tabBar.isHidden = true
    }
    
    
    // MARK: - Navigation controller related
    
    func showNavigationBar(animated: Bool = false) {
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func hideNavigationBar(animated: Bool = false) {
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func updateNavBarColor(tintColor: UIColor) {
        
        navigationController?.navigationBar.barTintColor = tintColor
    }
    
    
    // MARK: - NavigationBar related
    
    func setNoneNavBarButton(left: Bool? = false,
                             titleView: Bool? = false,
                             right: Bool? = false,
                             all: Bool? = false,
                             animated: Bool? = false) {
        
        if all! {
            
            navigationItem.setHidesBackButton(true, animated: animated!)
            navigationItem.setLeftBarButtonItems(nil, animated: animated!)
            navigationItem.titleView = nil
            navigationItem.setRightBarButtonItems(nil, animated: animated!)
            
        } else {
            
            if left! {
                
                navigationItem.setHidesBackButton(true, animated: animated!)
                navigationItem.setLeftBarButtonItems(nil, animated: animated!)
            }
            
            if titleView! {
                
                navigationItem.titleView = nil
            }
            
            if right! {
                
                navigationItem.setRightBarButtonItems(nil, animated: animated!)
            }
        }
    }
    
    func addsNavigationBarShadow() {
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = CustomColor.boxShadow45.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 1
    }
    
    func addsBackButton(_ targetDataModel: TargetModel) {
        
        addsLeftBarButton(image: CustomImage.NavBar.burger, targetsDataModel: [targetDataModel])
    }
    
    func addsLeftBarButton(image: UIImage, roundCorners: Bool? = false, targetsDataModel: [TargetModel]) {
        
        navigationItem.setHidesBackButton(true, animated: false)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(image, for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: AppConstant.navBarItemSize, height: AppConstant.navBarItemSize)
        backButton.widthAnchor.constraint(equalToConstant: AppConstant.navBarItemSize).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: AppConstant.navBarItemSize).isActive = true
        backButton.adjustsImageWhenHighlighted = false
        backButton.addTargets(targetsDataModel)
        
        if roundCorners == true {
            
            backButton.roundCorners()
        }
        
        let leftBarButton = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = leftBarButton
    }
    
    func addsRightBarButton(image: UIImage, targetsDataModel: [TargetModel]) {
        
        let closeButton = UIButton(type: .custom)
        closeButton.setImage(image, for: .normal)
        closeButton.frame = CGRect(x: 0, y: 0, width: AppConstant.navBarItemSize, height: AppConstant.navBarItemSize)
        closeButton.widthAnchor.constraint(equalToConstant: AppConstant.navBarItemSize).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: AppConstant.navBarItemSize).isActive = true
        closeButton.adjustsImageWhenHighlighted = false
        closeButton.addTargets(targetsDataModel)
        
        let rightBarButton = UIBarButtonItem(customView: closeButton)
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func addsRightBarButton(title: String, targetDataModel: TargetModel) {
        
        let itemModel = BarItemDataModel(title: title, targetDataModel: targetDataModel)
        addsRightBarButtons(items: [itemModel])
    }
    
    @discardableResult func addsRightBarButtons(items: [BarItemDataModel]) -> [UIButton] {
        
        var barButtonItems = [UIBarButtonItem]()
        var buttonsArray: [UIButton] = []
        
        for item in items {
            
            let button = UIButton(type: .custom)
            button.setImage(item.icon, for: .normal)
            button.setTitle(item.title, for: .normal)
            button.titleLabel?.font = CustomFont.regular8
            button.adjustsImageWhenHighlighted = false
            
            var itemSize = AppConstant.navBarItemSize
            
            if let text = item.title {
                
                itemSize = text.getStringSize(font: CustomFont.regular8, height: Int(AppConstant.navBarItemSize)).width
            }
            
            button.frame = CGRect(x: 0, y: 0, width: itemSize, height: itemSize)
            
            button.heightAnchor.constraint(equalToConstant: AppConstant.navBarItemSize).isActive = true
                        
            button.addTargets([item.targetDataModel])
            
            buttonsArray.append(button)
            
            barButtonItems.append(UIBarButtonItem(customView: button))
        }
        
        navigationItem.rightBarButtonItems = barButtonItems
        
        return buttonsArray
    }
    
    
    // MARK: - TabBar controller related
    
    func setTabBarIcon(title: String, image: UIImage, selectedImage: UIImage) {
        
        tabBarItem = UITabBarItem(title: title,
                                  image: image.withRenderingMode(.alwaysOriginal),
                                  selectedImage: selectedImage.withRenderingMode(.alwaysOriginal))
    }
    
    
    // MARK: - Check if view is in screen
    
    func isViewInScreen() -> Bool {
        
        return viewIfLoaded?.window != nil
    }
    
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Keyboard
    
    @objc func dismissKeyboard() {
        
        AppUtils.dismissKeyboard()
    }
    
    func dismissKeyboardWhenBackgroundTap(_ value: Bool? = true) {
        
        if value == true {
            
            addTapRecognizer()
            
        } else {
            
            removeTapRecognizer()
        }
    }
    
    private func addTapRecognizer() {
        
        if tapRecognizer == nil {
            
            tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            tapRecognizer!.cancelsTouchesInView = false
            view.addGestureRecognizer(tapRecognizer!)
        }
    }
    
    private func removeTapRecognizer() {
        
        if let tapRecognizer = tapRecognizer {
            
            view.removeGestureRecognizer(tapRecognizer)
            self.tapRecognizer = nil
        }
    }
    
    @objc func actionTriggered() {
        
    }
    
    // Show generic error
    
    func showGenericError(_ error: String) {
        
        let alertController = UIAlertController(title: localizable(.key_error), message: error, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: localizable(.key_ok), style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    // Show generic confirmation message
    
    func showGenericError(_ title: String, _ message: String ) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: localizable(.key_ok), style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}
