//
//  Strings+Utils.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit


extension String {
    
    func getStringSize(font: UIFont, height: Int) -> CGSize {
        
        let rect = self.boundingRect(with: CGSize(width:100000000, height: height), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return rect.size
    }
    
    func substring(toIndex index: Int) -> String? {
        
        if count >= index {
            
            let startIndex = self.index(self.startIndex, offsetBy: index)
            
            return String(self[..<startIndex])
        }
        
        return nil
    }
}


