//
//  NSObject+Utils.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

public final class ObjectAssociation<T: AnyObject> {
    
    private let policy: objc_AssociationPolicy
    
    /// - Parameter policy: An association policy that will be used when linking objects.
    public init(policy: objc_AssociationPolicy = .OBJC_ASSOCIATION_RETAIN_NONATOMIC) {
        
        self.policy = policy
    }
    
    /// Accesses associated object.
    /// - Parameter index: An object whose associated object is to be accessed.
    public subscript(index: AnyObject) -> T? {
        
        get { return objc_getAssociatedObject(index, Unmanaged.passUnretained(self).toOpaque()) as! T? }
        set { objc_setAssociatedObject(index, Unmanaged.passUnretained(self).toOpaque(), newValue, policy) }
    }
}

extension NSObject {
    
    // MARK: - Dispatch blocks
    
    func after(_ delay: TimeInterval, execute closure: @escaping () -> Void) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: closure)
    }
    
    
    // MARK: - Notification center
    
    func addObserver(_ notificationName: String, selector: Selector) {
        
        removeObserver(notificationName)
    }
    
    func removeObserver(_ notificationName: String) {
        
    }
    
    func addObserver(_ notificationName: NSNotification.Name, selector: Selector) {
        
        NotificationCenter.default.addObserver(self,
                                               selector: selector,
                                               name: notificationName,
                                               object: nil)
    }
    
    func removeObserver(_ notificationName: NSNotification.Name) {
        
        NotificationCenter.default.removeObserver(self,
                                                  name: notificationName,
                                                  object: nil)
    }
}
