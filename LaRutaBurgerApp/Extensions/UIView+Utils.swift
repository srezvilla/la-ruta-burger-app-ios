//
//  UIView+Utils.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

extension UIView {
    
    // MARK: - Properties
    
    private static let association = ObjectAssociation<AnyObject>()
    
    var userInfo: AnyObject? {
        
        get { return UIView.association[self] }
        set { UIView.association[self] = newValue }
    }
    
    
    // MARK: - Subviews
    
    func addAndFillSubview(_ view: UIView) {
        
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.fillSuperviewCompletely()
    }
    
    func removeAllViews() {
        
        self.subviews.forEach { $0.removeFromSuperview() }
    }
    
    
    // MARK: - Rotate
    
    func rotate(_ expanded: Bool, animated: Bool) {
        
        let angle: CGFloat = expanded ? CGFloat.pi : -2 * CGFloat.pi
        let rotationTransform = CGAffineTransform(rotationAngle: angle)
        
        if animated {
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                
                self.transform = rotationTransform
                
            }, completion: nil)
            
        } else {
            
            self.transform = rotationTransform
        }
    }
    
    
    // MARK: - Gradient
    
    func applyGradient(colours: [UIColor]) -> Void {
        
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.name = "gradientLayer"
        gradient.isHidden = true
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func hideGradient(_ hide: Bool, animated: Bool? = false) {
        
        if animated == true {
            
            UIView.transition(with: self,
                              duration: 0.4,
                              options: UIViewAnimationOptions.transitionCrossDissolve,
                              animations: {
                                
                                self.hideGradient(hide)
            }, completion: nil)
            
        } else {
            
            hideGradient(hide)
        }
    }
    
    private func hideGradient(_ hide: Bool) {
        
        for layer in self.layer.sublayers! {
            
            if layer.name == "gradientLayer" {
                
                layer.isHidden = hide
            }
        }
    }
       
    static var identifier: String {
            
        return String(describing: self)
    }
        
    static var nib:UINib {
            
        return UINib(nibName: identifier, bundle: nil)
    }
    
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
}
