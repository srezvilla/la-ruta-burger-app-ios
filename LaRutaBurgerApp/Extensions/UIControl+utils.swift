//
//  UIControl+utils.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

extension UIControl {
    
    func addTargets(_ targets: [TargetModel]) {
        
        for target in targets {
            
            self.addTarget(target.target, action: target.selector, for: target.controlEvent)
        }
    }
}
