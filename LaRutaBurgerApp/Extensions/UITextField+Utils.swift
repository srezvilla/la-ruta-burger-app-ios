//
//  UITextField+Utils.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    // MARK: - Keyboard Toolbar
    
    // MARK: One Button (Done by default)
    
    func addKeyboardButton(_ singleDataModel: BarButtonDataModel? = nil, alignToLeft: Bool? = false) {
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        
        let defaultAction = BarButtonDataModel(target: self,
                                               selector: #selector(dismissKeyboard),
                                               title: localizable(.key_done))
        
        let onDataModel = singleDataModel ?? defaultAction
        
        let button = UIBarButtonItem(title: onDataModel.title,
                                     style: onDataModel.style!,
                                     target: onDataModel.target,
                                     action: onDataModel.selector)
        
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil), button]
        
        if alignToLeft == true {
            
            toolbar.items?.reverse()
        }
        
        toolbar.sizeToFit()
        inputAccessoryView = toolbar
    }
    
    
    // MARK: Two Buttons (Cancel <-> Done by default)
    
    func addKeyboardTwoButtons(leftDataModel: BarButtonDataModel? = nil, rightDataModel: BarButtonDataModel? = nil) {
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        
        let defaultLeftAction = BarButtonDataModel(target: self,
                                                   selector: #selector(dismissKeyboard),
                                                   title: localizable(.key_cancel))
        let onLeftDataModel = leftDataModel ?? defaultLeftAction
        let onLeftButtonItem = UIBarButtonItem(title: onLeftDataModel.title,
                                               style: onLeftDataModel.style!,
                                               target: onLeftDataModel.target,
                                               action: onLeftDataModel.selector)
        
        let defaultRightAction = BarButtonDataModel(target: self,
                                                    selector: #selector(dismissKeyboard),
                                                    title: localizable(.key_done))
        let onRightDataModel = rightDataModel ?? defaultRightAction
        let onRightButtonItem = UIBarButtonItem(title: onRightDataModel.title,
                                                style: onRightDataModel.style!,
                                                target: onRightDataModel.target,
                                                action: onRightDataModel.selector)
        
        toolbar.items = [onLeftButtonItem,
                         UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
                         onRightButtonItem]
        toolbar.sizeToFit()
        inputAccessoryView = toolbar
    }
    
    // MARK: Default Action
    
    @objc func dismissKeyboard() {
        
        AppUtils.dismissKeyboard()
    }
}

extension UITextView {
    
    func addKeyboardTwoButtons(leftDataModel: BarButtonDataModel? = nil, rightDataModel: BarButtonDataModel? = nil) {
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        
        let defaultLeftAction = BarButtonDataModel(target: self,
                                                   selector: #selector(dismissKeyboard),
                                                   title: localizable(.key_cancel))
        let onLeftDataModel = leftDataModel ?? defaultLeftAction
        let onLeftButtonItem = UIBarButtonItem(title: onLeftDataModel.title,
                                               style: onLeftDataModel.style!,
                                               target: onLeftDataModel.target,
                                               action: onLeftDataModel.selector)
        
        let defaultRightAction = BarButtonDataModel(target: self,
                                                    selector: #selector(dismissKeyboard),
                                                    title: localizable(.key_done))
        let onRightDataModel = rightDataModel ?? defaultRightAction
        let onRightButtonItem = UIBarButtonItem(title: onRightDataModel.title,
                                                style: onRightDataModel.style!,
                                                target: onRightDataModel.target,
                                                action: onRightDataModel.selector)
        
        toolbar.items = [onLeftButtonItem,
                         UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
                         onRightButtonItem]
        toolbar.sizeToFit()
        inputAccessoryView = toolbar
    }
    
    @objc func dismissKeyboard() {
        
        AppUtils.dismissKeyboard()
    }
}
