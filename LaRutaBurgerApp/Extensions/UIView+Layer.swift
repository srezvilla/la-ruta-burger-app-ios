//
//  UIView+Layer.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
    
    func addBorderColor(_ color: UIColor? = UIColor.white, borderWidth: CGFloat? = 1) {
        
        layer.borderWidth = borderWidth!
        layer.borderColor = color!.cgColor
    }
    
    func roundCorners() {
        
        roundCornersWithRadius(frame.size.height/2)
    }
    
    func roundCornersWithRadius(_ radius: CGFloat) {
        
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
    func roundCorners(corners: UIRectCorner) {
        
        let radius: CGFloat = frame.size.height / 2
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func addShadow(shadowRadius: CGFloat? = 1, height: CGFloat? = 1) {
        
        self.layer.shadowColor = CustomColor.blackAlpha20.cgColor 
        self.layer.shadowOffset = CGSize(width: 0, height: height!)
        self.layer.shadowOpacity = 2.0
        self.layer.shadowRadius = shadowRadius!
        self.layer.masksToBounds = false
    }
    
    func setBorderColor(color: UIColor) {
        
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0,y: 0, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width,y: 0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - width, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
}
