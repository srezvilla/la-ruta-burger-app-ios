//
//  UIView+Constraints.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

// MARK: - Constraint dimensions

import UIKit


extension UIView {

    @discardableResult func constraintWidth(_ width: CGFloat) -> NSLayoutConstraint? {
        
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .width,
                                            relatedBy: .equal,
                                            toItem: nil,
                                            attribute: .notAnAttribute,
                                            multiplier: 1,
                                            constant: width)
        self.addConstraint(constraint)
        
        return constraint
    }

    @discardableResult func constraintHeight(_ height: CGFloat) -> NSLayoutConstraint? {
        
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .height,
                                            relatedBy: .equal,
                                            toItem: nil,
                                            attribute: .notAnAttribute,
                                            multiplier: 1,
                                            constant: height)
        self.addConstraint(constraint)
        
        return constraint
    }


    // MARK: - Constraint margins to superview

    @discardableResult func constraintTopMargin(_ margin: CGFloat) -> NSLayoutConstraint? {
        
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .top,
                                            relatedBy: .equal,
                                            toItem: self.superview,
                                            attribute: .top,
                                            multiplier: 1,
                                            constant: margin)
        self.superview?.addConstraint(constraint)
        
        return constraint
    }

    @discardableResult func constraintBottomMargin(_ margin: CGFloat) -> NSLayoutConstraint? {
        
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .bottom,
                                            relatedBy: .equal,
                                            toItem: self.superview,
                                            attribute: .bottom,
                                            multiplier: 1,
                                            constant: -margin)
        self.superview?.addConstraint(constraint)
        
        return constraint
    }

    @discardableResult func constraintLeftMargin(_ margin: CGFloat) -> NSLayoutConstraint? {
        
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: self.superview,
                                            attribute: .leading,
                                            multiplier: 1,
                                            constant: margin)
        self.superview?.addConstraint(constraint)
        
        return constraint
    }

    @discardableResult func constraintRightMargin(_ margin: CGFloat) -> NSLayoutConstraint? {
        
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .trailing,
                                            relatedBy: .equal,
                                            toItem: self.superview,
                                            attribute: .trailing,
                                            multiplier: 1,
                                            constant: -margin)
        self.superview?.addConstraint(constraint)
        
        return constraint
    }

    @discardableResult func fillSuperviewWithInsets(_ insets: UIEdgeInsets) -> Array<NSLayoutConstraint>? {
        
        guard let top = constraintTopMargin(insets.top) else {
            return nil
        }
        
        guard let left = constraintLeftMargin(insets.left) else {
            return nil
        }
        
        guard let bottom = constraintBottomMargin(insets.bottom) else {
            return nil
        }
        
        guard let right = constraintRightMargin(insets.right) else {
            return nil
        }
        
        return [top, left, bottom, right]
    }

    @discardableResult func fillSuperviewCompletely() -> Array<NSLayoutConstraint>? {
        
        return fillSuperviewWithInsets(UIEdgeInsets.zero)
    }


    // MARK: - Constraint to centers of superview

    @discardableResult func constraintCenterX(_ offset: CGFloat = 0) -> NSLayoutConstraint? {
        
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .centerX,
                                            relatedBy: .equal,
                                            toItem: self.superview,
                                            attribute: .centerX,
                                            multiplier: 1,
                                            constant: offset)
        self.superview?.addConstraint(constraint)
        
        return constraint
    }

    @discardableResult func constraintCenterY(_ offset: CGFloat = 0) -> NSLayoutConstraint? {
        
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .centerY,
                                            relatedBy: .equal,
                                            toItem: self.superview,
                                            attribute: .centerY,
                                            multiplier: 1,
                                            constant: offset)
        self.superview?.addConstraint(constraint)
        
        return constraint
    }


    // MARK: - Update constrains

    func updateBottomMargin(_ margin: CGFloat) {
        
        for constraint in constraints {
            
            if constraint.firstAttribute == .bottom || constraint.secondAttribute == .bottom {
                
                constraint.constant = margin
            }
        }
    }
}
