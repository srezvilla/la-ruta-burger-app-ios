//
//  BurgersViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 19/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class BurgersViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var burgerTable: UITableView!
    @IBOutlet weak var viewSearchConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblNoResults: UILabel!
    @IBOutlet weak var lblRestaurant: UILabel!
    @IBOutlet weak var btnRestaurantPicker: UIButton!
    
    
    // MARK: - Constants
    
    let segueIdentifier = "detailSegue"
    let segueCreateBurger = "segueCreateBurger"
    
    
    // MARK: - Properties
    
    var burgers: [BurgerDataModel] = []
    var filteredBurgers: [BurgerDataModel] = []
    var isFiltering: Bool = false
    var burgerSelected: BurgerDataModel?
    var restaurantSelected: RestaurantData?
    
    
    // MARK: - Override functions

    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        setupLabels()
        dataRequest()
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Setup
    
    private func setupTableView() {
        
        dismissKeyboardWhenBackgroundTap()
        burgerTable.delegate = self
        burgerTable.dataSource = self
        
        burgerTable.register(BurgerTableViewCell.nib, forCellReuseIdentifier: BurgerTableViewCell.identifier)
        
        viewSearchConstraint.constant = 0
        viewSearch.addShadow(shadowRadius: 6.0, height: 5.0)
        txtSearch.addBottomBorderWithColor(color: CustomColor.blackAlpha10, width: 1.0)
        btnRestaurantPicker.adjustsImageWhenHighlighted = false
        
    }
    
    
    
    private func setupNavigationBar() {
        
        addsLeftBarButton(image: CustomImage.NavBar.menu,targetsDataModel: [TargetModel(target: self, selector: #selector(goToMenu))])
        
        navigationController?.removeShadow()
        extendedLayoutIncludesOpaqueBars = true
        
        
        let searchItemDataModel = BarItemDataModel(icon: CustomImage.NavBar.searchIcon,
                                                   targetDataModel: TargetModel(target: self, selector: #selector(searchAction)))
        
        let plusBarItemDataModel = BarItemDataModel(icon: CustomImage.NavBar.plusIcon,
                                                    targetDataModel: TargetModel(target: self, selector: #selector(plusAction)))
        
        let barItemsDataModel: [BarItemDataModel] = [plusBarItemDataModel, searchItemDataModel]
        
        addsRightBarButtons(items: barItemsDataModel)
    }
    
    private func setupNavigationBar(search: Bool) {
        
        let searchItemDataModel = BarItemDataModel(icon: search ? CustomImage.NavBar.searchIconOrange : CustomImage.NavBar.searchIcon,
                                                   targetDataModel: TargetModel(target: self, selector: #selector(searchAction)))
        
        let plusBarItemDataModel = BarItemDataModel(icon: CustomImage.NavBar.plusIcon,
                                                    targetDataModel: TargetModel(target: self, selector: #selector(plusAction)))
        
        let barItemsDataModel: [BarItemDataModel] = [plusBarItemDataModel, searchItemDataModel]
        
        addsRightBarButtons(items: barItemsDataModel)
    }
    
    private func setupLabels() {
        
        navigationItem.title = localizable(.key_burgers)
        btnCancel.setTitle(localizable(.key_cancel), for: .normal)
        btnSearch.setTitle(localizable(.key_search), for: .normal)
        txtSearch.placeholder = localizable(.key_search)
    }
    
    // MARK: - Data request
    
    private func dataRequest() {
        
        dbManager.getBurgers() { (burgerData,error) in
            
            if error == nil {
                
                self.burgers = burgerData
                self.burgerTable.reloadData()
                
            } else {
                
                self.showGenericError(error!)
            }
        }
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nav = segue.destination as? BurgerDetailViewController {
            
            nav.burgerdata = burgerSelected
            
        } else if let nav = segue.destination as? BurgerRestaurantPickerViewController {
            
            nav.delegate = self
            nav.restaurantSelected = self.restaurantSelected
        }
    }
    
    @objc func goToMenu() {
        
        gotoMenu()
    }
    
    
    @objc func plusAction() {
        
        if !AppSession.anonymousUser {
            
            performSegue(withIdentifier: segueCreateBurger, sender: self)
            
        } else {
            
            showGenericError(localizable(.key_warning), localizable(.key_anonymousWarning))
        }
    }
    
    @objc func searchAction() {

        setupNavigationBar(search: true)
        viewSearchConstraint.constant = 200
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func cancelSearchAction(_ sender: Any) {
        
        isFiltering = false
        resetFilters()
        burgerTable.reloadData()
        
        setupNavigationBar(search: false)
        viewSearchConstraint.constant = 0
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        if restaurantSelected == nil && txtSearch.text == "" {
            
            isFiltering = false
            setupNavigationBar(search: false)
            
        } else {
            
            isFiltering = true
            
            filteredBurgers = burgers
            
            if restaurantSelected != nil {
                
                filteredBurgers = burgers.filter( { $0.restaurant == restaurantSelected!.reference } )
            }
            
            if txtSearch.text != "" {
                
                
                filteredBurgers = filteredBurgers.filter( { $0.name.uppercased().range(of: txtSearch.text!.uppercased()) != nil } )
            }
 
            burgerTable.reloadData()
        }
        
        viewSearchConstraint.constant = 0
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func resetFilters() {
        
        restaurantSelected = nil
        txtSearch.text = ""
        lblRestaurant.text = localizable(.key_restaurant)
    }
}
