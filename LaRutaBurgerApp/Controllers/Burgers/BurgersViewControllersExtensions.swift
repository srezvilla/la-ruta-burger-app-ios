//
//  BurgersViewControllersExtensions.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 23/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

extension BurgersViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let numRows = isFiltering ? filteredBurgers.count : burgers.count
        
        if isFiltering {
            
            lblNoResults.isHidden = numRows != 0
            
        } else {
            
            lblNoResults.isHidden = true
        }
        
        return numRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        let cell = tableView.dequeueReusableCell(withIdentifier: BurgerTableViewCell.identifier, for: indexPath) as! BurgerTableViewCell
        
        let data = isFiltering ? filteredBurgers[indexPath.row] : burgers[indexPath.row]
        
        cell.setupCell(burgerData: data)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        burgerSelected = isFiltering ? filteredBurgers[indexPath.row] : burgers[indexPath.row]
        
        performSegue(withIdentifier: segueIdentifier, sender: self)
    }
}

extension BurgersViewController: receiveRestaurantPickData {
    
    func passRestaurantData(data: RestaurantData) {
        
        restaurantSelected = data
        lblRestaurant.text = data.name
    }
}
