//
//  BurgerCreateViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 24/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit
import Photos

protocol receiveRestaurantPickData {
    
    func passRestaurantData(data: RestaurantData)
}

class BurgerCreateViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var btnAddImage: UIButton!
    @IBOutlet weak var imgBurger: UIImageView!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnRestaurant: UIButton!
    @IBOutlet weak var lblRestaurant: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageUpload: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    
    // MARK: - Constants
    
    let imagePicker = UIImagePickerController()
    
    // MARK: - Properties
    
    var restaurantData: RestaurantData?
    var imageData: Data?
    var allowAttachmentPick: Bool! = false
    
    
    // MARK: - Override functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        hideTabBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        showTabBar()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Setup
    
    private func setupView() {
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        navigationItem.title = localizable(.key_newBurger)

        extendedLayoutIncludesOpaqueBars = true
        navigationItem.setHidesBackButton(true, animated:true);
        
        dismissKeyboardWhenBackgroundTap()
        
        btnSend.roundCorners()
        btnCancel.setBorderColor(color: CustomColor.blackAlpha20)
        btnCancel.roundCorners()
        btnRestaurant.adjustsImageWhenHighlighted = false
        btnAddImage.setBorderColor(color: CustomColor.blackAlpha20)
        txtDescription.layer.borderColor = CustomColor.blackAlpha10.cgColor
        txtDescription.layer.borderWidth = 1.0
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        txtTitle.placeholder = localizable(.key_title)
        
        
        let nextDataModelTitle = BarButtonDataModel(target: self, selector: #selector(nextButtonTitle), title: localizable(.key_next))
        let nextDataModelDescription = BarButtonDataModel(target: self, selector: #selector(nextButtonDescription), title: localizable(.key_done))
        
        txtTitle.addKeyboardTwoButtons(rightDataModel: nextDataModelTitle)
        txtDescription.addKeyboardTwoButtons(rightDataModel: nextDataModelDescription)
        
        lblRestaurant.text = localizable(.key_restaurant)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        var keyBoardHeight:CGFloat = 0
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            keyBoardHeight = keyboardSize.height - 44
            
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, animations: {
            self.contentViewConstraint.constant = keyBoardHeight
            self.imageUpload.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.2, delay: 0, animations: {
            
            self.imageUpload.isHidden = false
            self.contentViewConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }

    
    @objc func nextButtonTitle() {
        
        txtDescription.becomeFirstResponder()
    }
    
    @objc func nextButtonDescription() {
        
        dismissKeyboard()
    }
    
    
    // MARK: - Actions
    
    @IBAction func sendAction(_ sender: Any) {
        
        if txtTitle.text == "" || txtDescription.text == "" || restaurantData == nil {
            
            showGenericError(localizable(.key_registerWarningText))
            
        } else {
            
            showLoader()
            
            let burgerData = BurgerDataModel.init(txtTitle.text!, txtDescription.text!, 0, restaurantData!.reference, restaurantData!.name, AppSession.userData!.fullName, "", "")
            
            dbManager.addBurger(reviewData: burgerData!) { (error,ref) in
                
                if error == nil {
                    
                    if self.imageData != nil && ref != nil {
                        
                        storegaeManager.addBurgerPhoto(data: self.imageData!, ref: ref!) { error in
                            
                            self.hideLoader()
                            self.popViewController()
                        }
                        
                    } else {
                        
                        self.hideLoader()
                    }
                    
                } else {
                    
                    self.hideLoader()
                    self.showGenericError(error!)
                }
            }
        }
        
    }
    
    @IBAction func addPhotoAction(_ sender: Any) {
        
            
            let actionSheetController = UIAlertController(title: nil,
                                                          message: nil,
                                                          preferredStyle: .actionSheet)
            
            let photoAction: UIAlertAction = UIAlertAction(title: localizable(.key_camera),
                                                           style: .default) {
                                                            action -> Void in
                                                            
                                                            CameraManager().requestAccess(completionHandler: { (granted: Bool) in
                                                                
                                                                if granted {
                                                                    
                                                                    self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                                                                    self.allowAttachmentPick = true
                                                                    self.present(self.imagePicker, animated: true)
                                                                    
                                                                } else {
                                                                    
                                                                    debugPrint("Without camera access permissions or camera is busy")
                                                                }
                                                            })
            }
            
            let photoLibraryAction: UIAlertAction = UIAlertAction(title: localizable(.key_gallery),
                                                                  style: .default) {
                                                                    action -> Void in
                                                                    
                                                                    
                                                                    PhotoLibraryManager().requestAccess(completionHandler: { (granted: Bool) in
                                                                        
                                                                        if granted {
                                                                            
                                                                            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                                                                            self.allowAttachmentPick = true
                                                                            self.present(self.imagePicker, animated: true)
                                                                            
                                                                        } else {
                                                                            
                                                                            debugPrint("Without photo library permissions")
                                                                        }
                                                                    })
            }
       
            let cancelAction: UIAlertAction = UIAlertAction(title: localizable(.key_cancel), style: .cancel) { action -> Void in }
            cancelAction.setValue(CustomColor.greyishBrownThree, forKey: "titleTextColor")
            
            actionSheetController.addAction(photoAction)
            actionSheetController.addAction(photoLibraryAction)
            actionSheetController.addAction(cancelAction)

            present(actionSheetController, animated: true)
    }
    

    @IBAction func cancelAction(_ sender: Any) {
        
        popViewController()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nav = segue.destination as? BurgerRestaurantPickerViewController {
            
            nav.delegate = self
            nav.restaurantSelected = self.restaurantData
        }
    }
}

extension BurgerCreateViewController: receiveRestaurantPickData {
    
    func passRestaurantData(data: RestaurantData) {
        
        restaurantData = data
        lblRestaurant.text = restaurantData?.name
    }
    
    
    
}

// MARK: - UIImagePickerController

extension BurgerCreateViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if self.allowAttachmentPick {
            
            self.allowAttachmentPick = false
            
            PhotoLibraryManager().imagePickerDidFinishPickingMediaWithInfo(info) { (image,data) in
                
                if image != nil {
                    

                    self.imageUpload.image = image!
                    self.imgBurger.isHidden = true
                    self.btnAddImage.setBorderColor(color: CustomColor.white)
                    
                    self.imageHeightConstraint.constant = self.btnAddImage.frame.height
                }
                
                if data != nil {
                    
                    self.imageData = data
                }
                
                picker.dismiss()
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss()
    }
    
    

}
