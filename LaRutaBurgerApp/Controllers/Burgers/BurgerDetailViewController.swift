//
//  BurgerDetailViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 24/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit
import MapKit

class BurgerDetailViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var constraintViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var lblRestaurantAddress: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var imgBurger: UIImageView!
    @IBOutlet weak var btnRestaurant: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    
    // MARK: - Constants
    
    let viewCoefficient:CGFloat = 0.48
    let mapSegue = "mapSegue"
    let restuarantSegue = "restaurantSegue"
    
    // MARK: - Properties
    
    var burgerdata: BurgerDataModel?
    var burgerImage: UIImage?
    var restaurantData: RestaurantData?
    
    
    // MARK: - Override functions
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupNavigationBar()
        setupView()

    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        hideTabBar()
        setupLabels()
        restaurandtDataRequest()
        getBurgerPhoto()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        showTabBar()
    }
    
    // MARK: - Setup
    
    private func setupNavigationBar() {
        
        addsLeftBarButton(image: CustomImage.NavBar.back,targetsDataModel: [TargetModel(target: self, selector: #selector(backAction))])
        
        navigationController?.removeShadow()
        navigationItem.title = burgerdata?.name
        extendedLayoutIncludesOpaqueBars = true
    }
    
    private func setupLabels() {
        
        lblUserName.textColor = CustomColor.blackAlpha80
        lblDescription.textColor = CustomColor.blackAlpha80
        lblRestaurantName.textColor = CustomColor.blackAlpha80
        lblRestaurantAddress.textColor = CustomColor.blackAlpha80
        
        if burgerdata != nil {
            
            lblUserName.text = burgerdata!.userName
            lblRestaurantName.text = burgerdata!.restaurantName
            lblDescription.text = burgerdata!.description
            lblRestaurantAddress.text = ""
        }
        
    }
    
    private func setupView() {
        
        constraintViewHeight.constant = screenHeight * viewCoefficient
        updateLikeButton()
        
        btnRestaurant.adjustsImageWhenHighlighted = false
        btnMap.adjustsImageWhenHighlighted = false
        btnLike.adjustsImageWhenHighlighted = false    
    }
    
    private func updateLikeButton() {
        
        if burgerdata != nil {
         
            let likeUser = AppSession.hasBurgerLike(reference: burgerdata!.reference)
            
            let likeImage: UIImage = likeUser ? CustomImage.Img.likeBurger : CustomImage.Img.likeBurgerEmpty
            
            likeButton.setImage(likeImage, for: .normal)
            
        }
    }
    
    // MARK: Data Requests
    
    private func restaurandtDataRequest() {
    
        if burgerdata != nil {
            
            dbManager.getRestaurant(reference: burgerdata!.restaurant) { (data,error) in
                
                if error == nil {
                    
                    self.restaurantData = data
                    
                    if self.restaurantData != nil {
                        
                        UIView.transition(with: self.view,
                                          duration: 0.2,
                                          options: .transitionCrossDissolve,
                                          animations: {
                                            
                                            self.lblRestaurantName.text = self.restaurantData!.name
                                            self.lblRestaurantAddress.text = self.restaurantData!.address
                        },
                                          completion: nil)

                    }
                    
                } else {
                    
                    self.showGenericError(error!)
                }
            }
        }

    
    }

    
    // MARK: - Actions
    
    @objc func backAction() {
        
        popViewController()
    }
    
    @IBAction func likeAction(_ sender: Any) {
        
        if AppSession.anonymousUser {
            
            showGenericError(localizable(.key_warning), localizable(.key_anonymousWarning))
            return
        }
        
        if burgerdata != nil {
            
            let likeUser = AppSession.hasBurgerLike(reference: burgerdata!.reference)
            
            if likeUser {
                
                burgerdata!.likes -= 1
                
            } else {
                
                burgerdata!.likes += 1
            }

            dbManager.updateBurger(reference: burgerdata!.reference, burgerData: burgerdata!.dictionaryRepresentation()) { error in
                
                if error != nil {
                    
                    self.showGenericError(error!)                    
                }
            }
            
            AppSession.userLikeBurger(reference: burgerdata!.reference, like: !likeUser) { error in
                
                if error != nil {
                    
                    self.showGenericError(error!)
                }
            }
        }
        
        updateLikeButton()
        
        
    }
    
    @IBAction func mapAction(_ sender: Any) {
        
        performSegue(withIdentifier: mapSegue, sender: self)
    }
    
    @IBAction func restaurantAction(_ sender: Any) {
        
        if restaurantData != nil {
            
            performSegue(withIdentifier: restuarantSegue, sender: self)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nav = segue.destination as? MapViewController {
            
            let location = CLLocation.init(latitude: restaurantData!.latitud, longitude: restaurantData!.longitud)
            AppSession.restaurantLoacation = location
            
            nav.tabMode = false
            
        } else if let nav = segue.destination as? RestaurantDetailViewController {
            
            nav.restaurantData = restaurantData!
        }
    }
    
    // MARK: - Get burger photo
    
    private func getBurgerPhoto() {
        
        if burgerdata != nil && burgerImage == nil {

            showLoader()
            
            storegaeManager.getBurgerPhoto(ref: burgerdata!.documentId) { (image,error) in
                
                self.hideLoader()
                
                if error == nil {
                    
                    self.burgerImage = UIImage(data: image!)
                    self.imgBurger.image = self.burgerImage
                }
            }
        }        
    }
    
    
}
