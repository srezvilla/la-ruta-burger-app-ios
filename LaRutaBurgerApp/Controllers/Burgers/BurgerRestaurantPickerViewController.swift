//
//  BurgerRestaurantPickerViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 25/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class BurgerRestaurantPickerViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Properties
    
    var restaurantData: [RestaurantData] = []
    var delegate: receiveRestaurantPickData?
    var restaurantSelected: RestaurantData?
    
    // MARK: - Override functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        hideTabBar()
        restaurantRequest()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        showTabBar()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Setup
    
    private func setupView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(RestaurantPickTableViewCell.nib, forCellReuseIdentifier: RestaurantPickTableViewCell.identifier)
    }
    
    private func setupNavigationBar() {
        
        addsLeftBarButton(image: CustomImage.NavBar.back,targetsDataModel: [TargetModel(target: self, selector: #selector(backAction))])
        
        navigationController?.removeShadow()
        navigationItem.title = localizable(.key_restaurant)
        extendedLayoutIncludesOpaqueBars = true
    }
    
    
    // MARK: - Data request
    
    private func restaurantRequest() {
        
        dbManager.getRestaurants() { (restaurants,error) in
            
            if error == nil {
                
                
                self.restaurantData = restaurants.sorted(by: { $0.name < $1.name })
                self.tableView.reloadData()
                
            } else {
                
                self.showGenericError(error!)
            }
        }
    }
    
    
    // MARK: - Actions
    
    @objc func backAction() {
        
        popViewController()
    }
    
}

extension BurgerRestaurantPickerViewController: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantPickTableViewCell.identifier, for: indexPath) as! RestaurantPickTableViewCell
        
        let cellData = self.restaurantData[indexPath.row]
        
        let isSelectedCell: Bool = restaurantSelected != nil && restaurantSelected!.reference == cellData.reference
        
        cell.setupCell(restaurantData: restaurantData[indexPath.row], isSelectedCell)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return restaurantData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let restaurantData = self.restaurantData[indexPath.row]
        
        if delegate != nil {
            
            delegate?.passRestaurantData(data: restaurantData)
            backAction()
        }
    }
}

