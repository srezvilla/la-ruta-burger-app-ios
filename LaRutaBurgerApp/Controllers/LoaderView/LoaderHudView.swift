//
//  LoaderHudView.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class LoaderHudView: UIView {
    
    let containerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 300, height: 300))
    let actIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
    
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.backgroundColor = CustomColor.white

        containerView.backgroundColor = CustomColor.white
        addSubview(containerView)
        
        
        let loaderGif = UIImage.gif(name: "burgerLoader")
        var loadingText = UIImage.init(named: "loading")
        
        if AppSession.appPreferredLanguage == .en {
            
            loadingText = UIImage.init(named: "loading_en")
        }
        
        let view: UIImageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 300, height: 300))
        let viewText: UIImageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 300, height: 300))
        
        view.image = loaderGif
        viewText.image = loadingText

        
        

        containerView.translatesAutoresizingMaskIntoConstraints = false

        containerView.constraintCenterX()
        containerView.constraintCenterY()

        containerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintCenterX()
        view.constraintCenterY(-50)
        view.constraintWidth(80)
        view.constraintHeight(80)
        
        containerView.addSubview(viewText)
        viewText.translatesAutoresizingMaskIntoConstraints = false
        viewText.constraintCenterX()
        viewText.constraintCenterY(20)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Public
    
    class func show(inViewController viewController: UIViewController) {
        
        if !isHudAlreadyInViewController(viewController: viewController) && !viewController.isBeingDismissed {
            
            let hudView = LoaderHudView(frame: viewController.view.bounds)
            hudView.containerView.constraintWidth(300)
            hudView.containerView.constraintHeight(300)

            viewController.view.addAndFillSubview(hudView)
        }
    }
    
    class func hide(inViewController viewController: UIViewController) {
        
        let hud = hudForViewController(viewController: viewController)
        
        if hud != nil {
            
            hud?.removeFromSuperview()
        }
    }
    
    
    // MARK: - Private
    
    private class func hudForViewController(viewController: UIViewController) -> LoaderHudView? {
        
        let subviews = viewController.view.subviews.reversed()
        
        for subview in subviews {
            
            if subview is LoaderHudView {
                
                return subview as? LoaderHudView
            }
        }
        
        return nil
    }
    
    private class func isHudAlreadyInViewController(viewController: UIViewController) -> Bool {
        
        let subviews = viewController.view.subviews.reversed()
        
        for subview in subviews {
            
            if subview is LoaderHudView {
                
                return true
            }
        }
        
        return false
    }
}
