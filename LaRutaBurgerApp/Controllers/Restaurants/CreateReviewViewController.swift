//
//  CreateReviewViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 22/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class CreateReviewViewController: UIViewController {

    
    // MARK: - IBOutlets
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewConstraint: NSLayoutConstraint!
    
    
    // MARK: - Properties
    
    var restaurantData: RestaurantData?
    var ranking: Int = 0
    
    // MARK: - Override functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        hideTabBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        showTabBar()
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Setup
    
    private func setupView() {
        
        if restaurantData != nil {
            
            navigationItem.title = restaurantData?.name
        }
        
        extendedLayoutIncludesOpaqueBars = true        
        navigationItem.setHidesBackButton(true, animated:true);
        
        btnCancel.roundCorners()
        btnSend.roundCorners()
        
        btnCancel.setTitle(localizable(.key_cancel), for: .normal)
        btnCancel.addBorderColor(CustomColor.blackAlpha10, borderWidth: 1.0)
        btnSend.setTitle(localizable(.key_send), for: .normal)
        
        txtDescription.layer.borderColor = CustomColor.blackAlpha10.cgColor
        txtDescription.layer.borderWidth = 1.0
        
        txtTitle.placeholder = localizable(.key_title)
        
        dismissKeyboardWhenBackgroundTap()

        
        let nextDataModelTitle = BarButtonDataModel(target: self, selector: #selector(nextButtonTitle), title: localizable(.key_next))
        let nextDataModelDescription = BarButtonDataModel(target: self, selector: #selector(nextButtonDescription), title: localizable(.key_done))

        txtTitle.addKeyboardTwoButtons(rightDataModel: nextDataModelTitle)
        txtDescription.addKeyboardTwoButtons(rightDataModel: nextDataModelDescription)
        
    }
    
    @objc func nextButtonTitle() {
        
        txtDescription.becomeFirstResponder()
    }
    
    @objc func nextButtonDescription() {
        
        dismissKeyboard()
    }

    
    private func setupStars(ranking: Int) {
        
        setupStar(starNumber: 1, ranking: ranking, starImage: imgStar1)
        setupStar(starNumber: 2, ranking: ranking, starImage: imgStar2)
        setupStar(starNumber: 3, ranking: ranking, starImage: imgStar3)
        setupStar(starNumber: 4, ranking: ranking, starImage: imgStar4)
        setupStar(starNumber: 5, ranking: ranking, starImage: imgStar5)
        
    }
    
    private func setupStar(starNumber: Int, ranking: Int, starImage: UIImageView){
        
        UIView.transition(with: self.view,
                          duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: {
                            
                            let rankingTranslate = ranking / 2
                            
                            if rankingTranslate < starNumber {
                                
                                if ranking % 2 == 0 {
                                    
                                    starImage.image = CustomImage.Img.emptyStarBig
                                    
                                } else {
                                    
                                    let difference = starNumber - rankingTranslate
                                    
                                    if difference <= 1 {
                                        
                                        starImage.image = CustomImage.Img.halfStarBig
                                        
                                    } else {
                                        
                                        starImage.image = CustomImage.Img.emptyStarBig
                                    }
                                }
                                
                            } else {
                                
                                starImage.image = CustomImage.Img.starBig
                                
                            }
        },
                          completion: nil)
        


    }
    
    // MARK: - Actions
    
    @IBAction func sendAction(_ sender: Any) {
        
        let title = txtTitle.text
        let description = txtDescription.text
        let userName = AppSession.userData?.getFullName() ?? ""
        let reference = restaurantData?.reference ?? ""
        
        if title == "" || description == "" {
            
            showGenericError(localizable(.key_registerWarningText))
            
        } else {
            
            self.showLoader()
            let reviewData = ReviewDataModel.init(userName, title!, description!, ranking, reference)
            
            dbManager.addReview(reviewData: reviewData!) { (error) in
                
                if error == nil, let reference = self.restaurantData?.reference {
                 
                    dbManager.getRestaurantReviews(reference: reference) { (reviews,error) in
                        
                        if error == nil {
                            
                            let newRanking = self.getRanking(reviewsData: reviews)
                            let newData = self.restaurantData?.dictionaryRepresentation(newRanking)
                            
                            dbManager.updateRestaurant(reference: self.restaurantData!.reference, restaurantData: newData!) { (error) in
                                
                                self.hideLoader()
                                self.popViewController()
                                
                                if error == nil {
                                    
                                    
                                } else {
                                    
                                    self.showGenericError(error!)
                                }
                                
                            }
                            
                        } else {
                            
                            self.hideLoader()
                            self.showGenericError(error!)
                        }
                        
                    }
                    
                } else {

                    self.hideLoader()
                    self.showGenericError(error!)
                }
            }
        }
        
    }
    
    private func getRanking(reviewsData: [ReviewDataModel]) -> Int {
        
        var ranking = 0
        let dataCount = reviewsData.count + 1
        
        if reviewsData.count > 0 {
            
            for review in reviewsData {
                
                ranking = ranking + review.ranking
            }
            
            ranking = ranking + self.ranking
            
            if ranking > 0 {
                
                ranking = ranking / dataCount
            }
        }
        
        return ranking
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        popViewController()
    }
    
    @IBAction func starAction1(_ sender: Any) {
        
        setupStars(ranking: 2)
        ranking = 2
    }
    
    @IBAction func starAction2(_ sender: Any) {
        
        setupStars(ranking: 4)
        ranking = 4
    }
    
    @IBAction func starAction3(_ sender: Any) {
        
        setupStars(ranking: 6)
        ranking = 6
    }
    
    @IBAction func starAction4(_ sender: Any) {
        
        setupStars(ranking: 8)
        ranking = 8
    }
    
    @IBAction func starAction5(_ sender: Any) {
        
        setupStars(ranking: 10)
        ranking = 10
    }
}
