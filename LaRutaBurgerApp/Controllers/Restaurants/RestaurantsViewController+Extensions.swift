//
//  RestaurantsViewController+Extensions.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 22/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

extension RestaurantsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantTableViewCell.identifier, for: indexPath) as! RestaurantTableViewCell
        
        let data = isFiltering ? filteredRestaurants[indexPath.row] : restaurants[indexPath.row]
        
        cell.setupCell(restaurantData: data)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let numRows: Int = isFiltering ? filteredRestaurants.count : restaurants.count
        
        if isFiltering {
            
           lblNoResults.isHidden = numRows != 0
            
        } else {
            
            lblNoResults.isHidden = true
        }

        return numRows
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedRestaurant = isFiltering ? filteredRestaurants[indexPath.row] : restaurants[indexPath.row]
        
        self.performSegue(withIdentifier: self.detailSegue, sender: self)
    }
    
    
    
}


