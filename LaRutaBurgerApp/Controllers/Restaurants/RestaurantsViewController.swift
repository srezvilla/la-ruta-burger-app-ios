//
//  RestaurantsViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 19/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class RestaurantsViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewSearh: UIView!
    @IBOutlet weak var viewSearchConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    @IBOutlet weak var lblNoResults: UILabel!
    
    
    // MARK: - Constants
    
    let detailSegue = "restaurantDetailSegue"
    
    
    // MARK: - Properties
    
    var restaurants: [RestaurantData] = []
    var filteredRestaurants: [RestaurantData] = []
    var selectedRestaurant: RestaurantData?
    var ranking: Int = 0
    var isFiltering: Bool = false
    
    
    // MARK: - Override functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        setupLabels()
        restaurantsRequest()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Setup
    
    private func setupTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RestaurantTableViewCell.nib, forCellReuseIdentifier: RestaurantTableViewCell.identifier)
        
        viewSearh.addShadow(shadowRadius: 6.0, height: 5.0)
        viewSearchConstraint.constant = 0
        
        txtSearch.addBottomBorderWithColor(color: CustomColor.blackAlpha10, width: 1.0)
        
        dismissKeyboardWhenBackgroundTap()

    }
    
    
    private func setupNavigationBar() {
        
        addsLeftBarButton(image: CustomImage.NavBar.menu,targetsDataModel: [TargetModel(target: self, selector: #selector(goToMenu))])
        
        addsRightBarButton(image: CustomImage.NavBar.searchIcon,targetsDataModel: [TargetModel(target: self, selector: #selector(searchAction))])
        
        navigationController?.removeShadow()
        extendedLayoutIncludesOpaqueBars = true
    }
    
    private func setupNavigationbar(search: Bool) {
        
        addsRightBarButton(image: search ? CustomImage.NavBar.searchIconOrange : CustomImage.NavBar.searchIcon,targetsDataModel: [TargetModel(target: self, selector: #selector(searchAction))])
    }
    
    private func setupLabels() {
        
        navigationItem.title = localizable(.key_restaurants)
        btnCancel.setTitle(localizable(.key_cancel), for: .normal)
        btnSearch.setTitle(localizable(.key_search), for: .normal)
        txtSearch.placeholder = localizable(.key_search)
        lblNoResults.text = localizable(.key_noResults)
        
        let doneDataModel = BarButtonDataModel(target: self, selector: #selector(searchDone), title: localizable(.key_done))
        
        txtSearch.addKeyboardTwoButtons(rightDataModel: doneDataModel)
    }
    
    // MARK - Data requests
    
    func restaurantsRequest() {
        
        dbManager.getRestaurants() { (restaurants, error) in
            
            if error == nil {
                
                self.restaurants = restaurants.sorted(by: { $0.name < $1.name })

                self.tableView.reloadData()
                
                
            } else {
                
                self.showGenericError(error!)
            }
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction func cancelSearcAction(_ sender: Any) {
        
        isFiltering = false
        resetFilters()
        tableView.reloadData()
        
        setupNavigationbar(search: false)
        viewSearchConstraint.constant = 0
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func resetFilters() {
        
        ranking = 0
        setupStars(ranking: 0)
        txtSearch.text = ""
    }
    
    @objc func searchDone() {
        
        dismissKeyboard()
    }
    
    @IBAction func starAction1(_ sender: Any) {
        
        setupStars(ranking: 2)
        ranking = 2
    }
    
    @IBAction func starAction2(_ sender: Any) {
        
        setupStars(ranking: 4)
        ranking = 4
    }
    
    @IBAction func starAction3(_ sender: Any) {
        
        setupStars(ranking: 6)
        ranking = 6
    }
    
    @IBAction func starAction4(_ sender: Any) {
        
        setupStars(ranking: 8)
        ranking = 8
    }
    
    @IBAction func starAction5(_ sender: Any) {
        
        setupStars(ranking: 10)
        ranking = 10
    }
    
    private func setupStars(ranking: Int) {
        
        setupStar(starNumber: 1, ranking: ranking, starImage: imgStar1)
        setupStar(starNumber: 2, ranking: ranking, starImage: imgStar2)
        setupStar(starNumber: 3, ranking: ranking, starImage: imgStar3)
        setupStar(starNumber: 4, ranking: ranking, starImage: imgStar4)
        setupStar(starNumber: 5, ranking: ranking, starImage: imgStar5)
        
    }
    
    private func setupStar(starNumber: Int, ranking: Int, starImage: UIImageView){
        
        UIView.transition(with: self.view,
                          duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: {
                            
                            let rankingTranslate = ranking / 2
                            
                            if rankingTranslate < starNumber {
                                
                                if ranking % 2 == 0 {
                                    
                                    starImage.image = CustomImage.Img.emptyStarBig
                                    
                                } else {
                                    
                                    let difference = starNumber - rankingTranslate
                                    
                                    if difference <= 1 {
                                        
                                        starImage.image = CustomImage.Img.halfStarBig
                                        
                                    } else {
                                        
                                        starImage.image = CustomImage.Img.emptyStarBig
                                    }
                                }
                                
                            } else {
                                
                                starImage.image = CustomImage.Img.starBig
                                
                            }
        },
                          completion: nil)
        
        
        
    }
    
    
    //MARK: - Navigation
    
    @objc func goToMenu() {
        
        gotoMenu()
    }
    
    @objc func searchAction() {
        
        setupNavigationbar(search: true)
        viewSearchConstraint.constant = 200
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func performSearch(_ sender: Any) {
        
        if ranking == 0 && txtSearch.text == "" {
            
            isFiltering = false
            setupNavigationbar(search: false)
            
        } else {
            
            isFiltering = true
            
            filteredRestaurants = restaurants.filter( { $0.ranking >= self.ranking } )
            
            if txtSearch.text != "" {
                
                filteredRestaurants = filteredRestaurants.filter( { $0.name.uppercased().range(of: txtSearch.text!.uppercased()) != nil } )
            }
            
            tableView.reloadData()
        }
        
        viewSearchConstraint.constant = 0
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nav = segue.destination as? RestaurantDetailViewController {
            
            nav.restaurantData = selectedRestaurant
        }
    }
    
}
