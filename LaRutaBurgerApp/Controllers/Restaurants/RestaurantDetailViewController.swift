//
//  RestaurantDetailViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 22/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit
import MapKit

class RestaurantDetailViewController: UIViewController {

    
    // MARK: - IBOutlets
    
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var btnLink: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var viewSeparator1: UIView!
    @IBOutlet weak var viewSeparator2: UIView!
    @IBOutlet weak var viewConnstraint: NSLayoutConstraint!
    
    
    // MARK: - Constants
    
    let mapSegue = "mapSegue"
    
    
    // MARK: - Properties
    
    let viewCoefficient:CGFloat = 0.37
    var restaurantData: RestaurantData?
    var restaurantImage: UIImage?
    var reviews: [ReviewDataModel] = []
    
    
    // MARK: - Override functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        setupLabels()
        hideTabBar()
        getRestaurantPhoto()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        showTabBar()
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: Setup view
    
    private func setupLabels() {
        
        btnReview.setTitle(localizable(.key_reviewTitle), for: .normal)
    }
    
    private func setupView() {
        
        viewConnstraint.constant = screenHeight * viewCoefficient
        
        if let resturantTitle = restaurantData?.name {
            
                    navigationItem.title = resturantTitle
        }
        
        extendedLayoutIncludesOpaqueBars = true
        
        
        if let ranking = restaurantData?.ranking {
            
            setupStars(ranking: ranking)
        }
                
        btnMap.adjustsImageWhenHighlighted = false
        btnLink.adjustsImageWhenHighlighted = false
        btnShare.adjustsImageWhenHighlighted = false
        
        if let address = restaurantData?.address {
            
             lblAddress.text = address
            
        } else {
            
            lblAddress.text = localizable(.key_notAvailable)
        }
        
        if let description = restaurantData?.description, description != "" {
            
            lblDescription.text = description
            lblDescription.textAlignment = .justified
            
        } else {
            
            lblDescription.text = localizable(.key_notAvailable)
            lblDescription.textAlignment = .center
        }
        
        viewSeparator1.backgroundColor = CustomColor.blackAlpha20
        viewSeparator2.backgroundColor = CustomColor.blackAlpha20
        
        btnLink.isEnabled = restaurantData?.web != nil && restaurantData?.web != ""
        btnShare.isEnabled = restaurantData?.share != nil && restaurantData?.share != ""
    }
    
    private func setupNavigationBar() {
        
        addsLeftBarButton(image: CustomImage.NavBar.back,targetsDataModel: [TargetModel(target: self, selector: #selector(backAction))])
        
        navigationController?.removeShadow()
    }
    
    private func setupStars(ranking: Int) {
        
        setupStar(starNumber: 1, ranking: ranking, starImage: imgStar1)
        setupStar(starNumber: 2, ranking: ranking, starImage: imgStar2)
        setupStar(starNumber: 3, ranking: ranking, starImage: imgStar3)
        setupStar(starNumber: 4, ranking: ranking, starImage: imgStar4)
        setupStar(starNumber: 5, ranking: ranking, starImage: imgStar5)
        
    }
    
    private func setupStar(starNumber: Int, ranking: Int, starImage: UIImageView){
        
        let rankingTranslate = ranking / 2
        
        if rankingTranslate < starNumber {
            
            if ranking % 2 == 0 {
                
                starImage.image = CustomImage.Img.emptyStarBig
                
            } else {
                
                let difference = starNumber - rankingTranslate
                
                if difference <= 1 {
                    
                    starImage.image = CustomImage.Img.halfStarBig
                    
                } else {
                    
                    starImage.image = CustomImage.Img.emptyStarBig
                }
            }
            
        } else {
            
            starImage.image = CustomImage.Img.starBig        }
    }
    
    
    // MARK: - Action
    
    @IBAction func mapAction(_ sender: Any) {
        
        performSegue(withIdentifier: mapSegue, sender: self)
    }
    
    @IBAction func linkAction(_ sender: Any) {
        
        if let link = restaurantData?.web {
            
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func shareAction(_ sender: Any) {
        
        if let link = restaurantData?.share {
            
            let textToShare = [ link ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
        
    }
    
    private func dataRequest() {
        
        if let referrence = restaurantData?.reference {
            
            dbManager.getRestaurantReviews(reference: referrence) { (reviews,error) in
                
                if error == nil {
                    
                    self.reviews = reviews
                    let ranking = self.getRanking(reviewsData: self.reviews)
                    self.setupStars(ranking:ranking)
                    
                    dbManager.updateRestaurant(reference: self.restaurantData!.reference, restaurantData: self.restaurantData!.dictionaryRepresentation(ranking)) { (error) in
                        
                        
                    }
                    
                } else {
                    
                    self.showGenericError(error!)
                }
            }
        }
    }
    
    private func getRanking(reviewsData: [ReviewDataModel]) -> Int {
        
        var ranking = 0
        let dataCount = reviewsData.count
        
        if reviewsData.count > 0 {
            
            for review in reviewsData {
                
                ranking = ranking + review.ranking
            }
            
            if ranking > 0 {
                
                ranking = ranking / dataCount
            }
        }
                
        return ranking
    }
    
    
    // MARK: Navigation
    
    @objc func backAction() {
    
        popViewController()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nav = segue.destination as? RestaurantReviewsViewController {
            
            nav.restaurantData = restaurantData
            
        } else if let nav = segue.destination as? MapViewController {
                        
            let location = CLLocation.init(latitude: restaurantData!.latitud, longitude: restaurantData!.longitud)
            AppSession.restaurantLoacation = location
            
            nav.tabMode = false
        }
    }

    
    // MARK: - Get restaurant photo
    
    private func getRestaurantPhoto() {
        
        
        if restaurantData != nil && restaurantImage == nil {
            
            showLoader()
            
            storegaeManager.getRestaurantPhoto(ref: restaurantData!.documentId) { (image,error) in
                
                self.hideLoader()
                
                if error == nil {
                    
                    self.restaurantImage = UIImage(data: image!)
                    self.imgMain.image = self.restaurantImage
                }
            }
        }
 
    }

}
