//
//  RestaurantReviewsViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 22/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class RestaurantReviewsViewController: UIViewController {

    
    // MARK: - IBOutlets
    
    @IBOutlet weak var reviewsTable: UITableView!
    @IBOutlet weak var lblNoResults: UILabel!
    
    
    // MARK: - Properties
    
    var reviews: [ReviewDataModel] = []
    var restaurantData: RestaurantData?

    
    // MARK: - Constantas
    
    let createReviewSegue = "createReviewSegue"
    
    
    // MARK: - Override functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupTableView()
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        dataRequest()
        hideTabBar()
        setupLabels()
    }

    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        showTabBar()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Setup
    
    private func setupTableView() {
        
        reviewsTable.delegate = self
        reviewsTable.dataSource = self
        reviewsTable.register(ReviewTableViewCell.nib, forCellReuseIdentifier: ReviewTableViewCell.identifier)

    }
    
    private func setupLabels() {
        
        lblNoResults.text = localizable(.key_reviewNoResults)
    }
    
    private func setupNavigationBar() {
        
        addsLeftBarButton(image: CustomImage.NavBar.back,targetsDataModel: [TargetModel(target: self, selector: #selector(backAction))])
        
        addsRightBarButton(image: CustomImage.NavBar.editIcon,targetsDataModel: [TargetModel(target: self, selector: #selector(editAction))])
        
        navigationController?.removeShadow()
        
        if let resturantTitle = restaurantData?.name {
            
            navigationItem.title = resturantTitle
        }
        
        extendedLayoutIncludesOpaqueBars = true
    }
    
    
    // MARK: - Data request
    
    private func dataRequest() {
        
        if let referrence = restaurantData?.reference {
            
            dbManager.getRestaurantReviews(reference: referrence) { (reviews,error) in
                
                if error == nil {
                    
                    self.lblNoResults.isHidden = reviews.count > 0
                    self.reviews = reviews
                    self.reviewsTable.reloadData()
                    
                } else {
                    
                    self.showGenericError(error!)
                }
            }
        }
    }
    
    // MARK: Navigation
    
    @objc func backAction() {
        
        popViewController()
    }
    
    
    @objc func editAction() {
        
        if !AppSession.anonymousUser {
            
            performSegue(withIdentifier: createReviewSegue, sender: self)
            
        } else {
            
            showGenericError(localizable(.key_warning), localizable(.key_anonymousWarning))
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nav = segue.destination as? CreateReviewViewController {
            
            nav.restaurantData = restaurantData
        }
    }

}
