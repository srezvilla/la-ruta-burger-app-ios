//
//  MapView+Extensions.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 20/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import MapKit

extension MapViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error) {
        
        AppSession.userCordinate = defaultUserLocation
    }
    
}
