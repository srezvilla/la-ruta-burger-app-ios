//
//  MapViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 19/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var burgerMap: MKMapView!
    
    
    // MARK: - Constants
    
    let locationManager = CLLocationManager()
    let segueIdentifier = "detailSegue"
    
    
    // MARK: - Properties
    
    var resturants: [RestaurantData] = []
    var selectedRestaurant: RestaurantData?
    var locationSelected: CLLocation?
    var tabMode:Bool = true
    
    
    // MARK: - Override functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupNavigationBar()
        burgerMap.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        restaurantsRequest()
        checkLocationAuthorizationStatus()
        setupLabels()
        
        if !tabMode {
            
            hideTabBar()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        showTabBar()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Setup
    
    private func setupNavigationBar() {
        
        if tabMode {
            
            addsLeftBarButton(image: CustomImage.NavBar.menu,targetsDataModel: [TargetModel(target: self, selector: #selector(goToMenu))])
            
        } else {
            
            addsLeftBarButton(image: CustomImage.NavBar.back,targetsDataModel: [TargetModel(target: self, selector: #selector(backAction))])
        }

        
        navigationController?.removeShadow()
        extendedLayoutIncludesOpaqueBars = true
    }
    
    private func setupLabels() {
        
        navigationItem.title = localizable(.key_burgerMap)
    }
    
    // MARK - Data requests
    
    func restaurantsRequest() {
        
        dbManager.getRestaurants() { (restaurants, error) in
            
            if error == nil {
                
                self.resturants = restaurants.sorted(by: { $0.ranking > $1.ranking })
                self.drawMarkersOnMap(restaurants: restaurants)
                
            } else {
                
                self.showGenericError(error!)
            }
        }
    }
    
    
    // MARK: - Navigation
    
    @objc func goToMenu() {
        
        gotoMenu()
    }
    
    // MARK: - MapView Annotations
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            
            let anView = MKAnnotationView(frame: CGRect.init(x: 0, y: 0, width: 78, height: 78))
            anView.image = CustomImage.Icon.userLocation
            return anView
            
        } else {
            
            let anView = MKAnnotationView(frame: CGRect.init(x: 0, y: 0, width: 60, height: 80))
            anView.image = CustomImage.Img.burgerMarker
            
            anView.canShowCallout = true
            anView.calloutOffset = CGPoint(x: -5, y: 5)
            let btnDetails = UIButton(type: .custom)
            btnDetails.setImage(CustomImage.Icon.rightButtonLocation, for: .normal)
            btnDetails.frame = CGRect(x: 0, y: 0, width: 18, height: 28)
            btnDetails.addTarget(self, action: #selector(didSelectAnnotation), for: .touchUpInside)
            
            let index = getAnnotationIndex(title: annotation.title! ?? "")
            btnDetails.tag = index
            anView.rightCalloutAccessoryView = btnDetails
            
            switch index {
                case 0: anView.image = CustomImage.Img.goldMarker
                case 1: anView.image = CustomImage.Img.silverMarker
                case 2: anView.image = CustomImage.Img.bronzeMarker
                default: anView.image = CustomImage.Img.burgerMarker
            }
            
            let subtitleView = UILabel()
            subtitleView.numberOfLines = 2
            subtitleView.text = annotation.subtitle!
            subtitleView.font = CustomFont.regular12
            anView.detailCalloutAccessoryView = subtitleView

            
            
            return anView
        }

    }
    
    @objc func didSelectAnnotation(sender:UIButton) {
        
        let tag = sender.tag
        
        if tag < resturants.count {
            
            selectedRestaurant = resturants[tag]
            
            performSegue(withIdentifier: segueIdentifier, sender: self)
        }
    }
    
    private func getAnnotationIndex(title: String) -> Int{
        
        var index = 0
        
        for restaurant in resturants {
            
            if restaurant.name == title {
                
                break
                
            } else {
                
                index += 1
            }
        }
        
        return index
    }
    
    // MARK: - Markers
    
    func drawMarkersOnMap(restaurants: [RestaurantData]) {
        
        let annotations = burgerMap.annotations
        burgerMap.removeAnnotations(annotations)
        
        for restaurant in restaurants {
            
            var annotations: [MKPointAnnotation] = []
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(
                latitude: restaurant.latitud,
                longitude: restaurant.longitud)
            annotation.title = restaurant.name
            annotation.subtitle = restaurant.address
            annotations.append(annotation)
            
            burgerMap.addAnnotations(annotations)
        }
    }
    
    @IBAction func centerLocationAction(_ sender: UIButton) {
        
        centerMapOnUserLocation()
    }
    
    
    
    // MARK: - Center Map
    
    func centerMapOnUserLocation() {
        
        var mapRegion = MKCoordinateRegion()
        
        if AppSession.restaurantLoacation != nil {
            
            mapRegion.center = (AppSession.restaurantLoacation!.coordinate)
            mapRegion.span.latitudeDelta = 0.005
            mapRegion.span.longitudeDelta = 0.005
            AppSession.restaurantLoacation = nil
            
        } else if let location = locationManager.location {
            
            
            mapRegion.center = (location.coordinate)
            mapRegion.span.latitudeDelta = 0.1
            mapRegion.span.longitudeDelta = 0.1
            
        } else {
            
            mapRegion.center = (AppSession.userCordinate.coordinate)
            mapRegion.span.latitudeDelta = 0.1
            mapRegion.span.longitudeDelta = 0.1
        }
        
        burgerMap.setRegion(mapRegion, animated: true)
    }

    // MARK: - check Location access
    
    func checkLocationAuthorizationStatus() {
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse && CLLocationManager.locationServicesEnabled() {
            
            burgerMap.showsUserLocation = true
            
            if locationManager.location != nil {
                
                AppSession.userCordinate = locationManager.location!
            }
            
        } else {
            
            locationManager.requestWhenInUseAuthorization()
        }
        
        centerMapOnUserLocation()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nav = segue.destination as? RestaurantDetailViewController {
            
            nav.restaurantData = selectedRestaurant
        }
    }
    
    @objc func backAction() {
        
        popViewController()
    }
    
}
