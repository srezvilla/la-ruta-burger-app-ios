//
//  RankingViewController+extensions.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 25/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

extension RankingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if sgmTable.selectedSegmentIndex == 0 {
            
            return restaurants.count
            
        } else {
            
            return burgers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if sgmTable.selectedSegmentIndex == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantRankTableViewCell.identifier) as! RestaurantRankTableViewCell
            
            let row = indexPath.row
            
            cell.setupCell(restaurantData: restaurants[row], index: row)
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BurgerRankTableViewCell.identifier) as! BurgerRankTableViewCell
            
            let row = indexPath.row
            
            cell.setupCell(burgerData: burgers[row], index: row)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        
        if sgmTable.selectedSegmentIndex == 0 {
            
            restaurantSelected = restaurants[row]
            performSegue(withIdentifier: restaurantSegue, sender: self)
            
        } else {
            
            burgerSelected = burgers[row]
            performSegue(withIdentifier: burgerSegue, sender: self)
        }
        
        
    }
}
