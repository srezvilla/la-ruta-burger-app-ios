//
//  FirstViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 14/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class RankingViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sgmTable: UISegmentedControl!
    
    
    // MARK: - Constatnts
    
    let restaurantSegue = "restaurantSegue"
    let burgerSegue = "burgerSegue"
    
    
    // MARK: - Properties
    
    var restaurants: [RestaurantData] = []
    var burgers: [BurgerDataModel] = []
    var restaurantSelected: RestaurantData?
    var burgerSelected: BurgerDataModel?
    
    
    // MARK: - View Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        getRestaurants()
        getBurgers()
    }
    
    private func setupView() {
        
        extendedLayoutIncludesOpaqueBars = true
        setupNavigationBar()
        setupTableView()
        
        sgmTable.setTitle(localizable(.key_restaurants), forSegmentAt: 0)
        sgmTable.setTitle(localizable(.key_burgers), forSegmentAt: 1)
    }
    
    // MARK: - Setup TableView
    
    private func setupTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self 
        tableView.register(RestaurantRankTableViewCell.nib, forCellReuseIdentifier: RestaurantRankTableViewCell.identifier)
                tableView.register(BurgerRankTableViewCell.nib, forCellReuseIdentifier: BurgerRankTableViewCell.identifier)
    }
    
    
    // MARK: Setup Navigation Bar
    
    private func setupNavigationBar() {
        
        navigationItem.title = localizable(.key_topRanking)
        
        addsLeftBarButton(image: CustomImage.NavBar.menu,targetsDataModel: [TargetModel(target: self, selector: #selector(goToMenuAction))])
                navigationController?.removeShadow()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @objc func goToMenuAction() {

        gotoMenu()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nav = segue.destination as? RestaurantDetailViewController {
            
           nav.restaurantData = restaurantSelected
            
        } else if let nav = segue.destination as? BurgerDetailViewController {
            
            nav.burgerdata = burgerSelected
        }
    }
    
    // MARK: - Actions
    
    @IBAction func sgmAction(_ sender: Any) {
        
        tableView.reloadData()
    }

    // MARK: - Data requests
    
    private func getRestaurants() {
        
        dbManager.getRestaurants() { (data,error) in
            
            if error == nil {
                
                self.restaurants = data.sorted(by: { $0.ranking > $1.ranking })
                    
                
                self.tableView.reloadData()
                
            } else {
                
                self.showGenericError(error!)
            }
            
        }
    }
    
    private func getBurgers() {
        
        dbManager.getBurgers() { (data,error) in
            
            if error == nil {
                
                self.burgers = data.sorted(by: { $0.likes > $1.likes })
                
            } else {
                
               self.showGenericError(error!)
            }
        }
    }
}

