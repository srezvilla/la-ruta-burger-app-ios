//
//  NewUserViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class NewUserViewController: UIViewController {

    
    // MARK: - IBOutlets
    
    @IBOutlet weak var lblMainTitle: UILabel!
    @IBOutlet weak var txtEmailInput: UITextField!
    @IBOutlet weak var txtNameInput: UITextField!
    @IBOutlet weak var txtPasswordInput: UITextField!
    @IBOutlet weak var txtRepeatPasswordInput: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIStackView!
    
    
    // MARK: - Constants
    
    let loginPassSegue = "loginSegue"
    
    
    // MARK: - Setup view
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        setupLabels()
    }
    

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    func setupLabels() {
        
        lblMainTitle.text = localizable(.key_newUser)
        txtEmailInput.placeholder = localizable(.key_email)
        txtNameInput.placeholder = localizable(.key_fullName)
        txtPasswordInput.placeholder = localizable(.key_password)
        txtRepeatPasswordInput.placeholder = localizable(.key_repeatPassword)
        btnCancel.setTitle(localizable(.key_cancel), for: .normal)
        btnLogin.setTitle(localizable(.key_login), for: .normal)
    }
    
    func setupView() {
        
        dismissKeyboardWhenBackgroundTap()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        txtEmailInput.addBottomBorderWithColor(color: CustomColor.blackAlpha10, width: 1.0)
        txtEmailInput.setLeftPaddingPoints(10)
        txtNameInput.addBottomBorderWithColor(color: CustomColor.blackAlpha10, width: 1.0)
        txtNameInput.setLeftPaddingPoints(10)
        txtPasswordInput.addBottomBorderWithColor(color: CustomColor.blackAlpha10, width: 1.0)
        txtPasswordInput.setLeftPaddingPoints(10)
        txtRepeatPasswordInput.addBottomBorderWithColor(color: CustomColor.blackAlpha10, width: 1.0)
        txtRepeatPasswordInput.setLeftPaddingPoints(10)
        
        btnCancel.addBorderColor(CustomColor.blackAlpha10, borderWidth: 1.0)
        btnCancel.roundCorners()
        btnLogin.roundCorners()
        
        let nextDataModelEmail = BarButtonDataModel(target: self, selector: #selector(nextButtonEmail), title: localizable(.key_next))
        let nextDataModelName = BarButtonDataModel(target: self, selector: #selector(nextButtonName), title: localizable(.key_next))
        let nextDataModelPassword = BarButtonDataModel(target: self, selector: #selector(nextButtonPassword), title: localizable(.key_next))
        let nextDataModelRepeat = BarButtonDataModel(target: self, selector: #selector(nextButtonRepeatPassword), title: localizable(.key_done))
        
        txtEmailInput.addKeyboardTwoButtons(rightDataModel: nextDataModelEmail)
        txtNameInput.addKeyboardTwoButtons(rightDataModel: nextDataModelName)
        txtPasswordInput.addKeyboardTwoButtons(rightDataModel: nextDataModelPassword)
        txtRepeatPasswordInput.addKeyboardTwoButtons(rightDataModel: nextDataModelRepeat)
    }
    
    // MARK: - Actions
    
    @objc func nextButtonEmail(){
        
        txtNameInput.becomeFirstResponder()
    }
    
    @objc func nextButtonName(){
        
        txtPasswordInput.becomeFirstResponder()
    }
    
    @objc func nextButtonPassword(){
        
        txtRepeatPasswordInput.becomeFirstResponder()
    }
    
    @objc func nextButtonRepeatPassword(){
        
        dismissKeyboard()
    }

    @IBAction func newUserAction(_ sender: UIButton) {
        
        let email = txtEmailInput.text
        let name = txtNameInput.text
        let pass = txtPasswordInput.text
        let repeatPass = txtRepeatPasswordInput.text
        
        if email == "" || name == "" || pass == "" || repeatPass == "" {
            
            showGenericError(localizable(.key_registerWarningText))
            
        } else if pass != repeatPass {
            
            showGenericError(localizable(.key_registerWarningText2))
            
        } else {
            
            showLoader()
            
            AppSession.newUser(email!, name!, pass!) { (error) in
                
                if error == nil {
                    
                    var userData = UserData.init(name!, email!, [], "")
                    
                    
                    dbManager.addUser(userData: userData!) { (reference,error) in
                        
                        if reference != nil {
                            
                            userData?.reference = reference!
                        }
                        
                        if error == nil {
                            
                            let alert = UIAlertController(title: localizable(.key_newUser),
                                                          message: localizable(.key_newUserConfirmation),
                                                          preferredStyle: .alert)
                            
                            let okAction = UIAlertAction(title: localizable(.key_ok), style: .default) { action in
                                
                                if userData != nil  {
                
                                        if error == nil {
                                            
                                            AppSession.logIn(email!, pass!, completion:{ error in
                                                
                                                self.hideLoader()
                                                
                                                if error == nil {
                                                    
                                                    self.performSegue(withIdentifier: self.loginPassSegue, sender: self)
                                                } else {
                                                    
                                                    self.showGenericError(error!)
                                                }
                                            })
                                            
                                        } else {
                                            
                                            self.hideLoader()
                                            self.showGenericError(error!)
                                        }
                                    
                                } else  {
                                    
                                    self.hideLoader()
                                    self.showGenericError(localizable(.key_error))
                                }
                            }
                            
                            alert.addAction(okAction)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                        } else {
                            
                            self.hideLoader()
                            self.showGenericError(error!)
                        }
                    }
                    
                } else {
                    
                    self.hideLoader()
                    self.showGenericError(error!)
                }
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        var keyBoardHeight:CGFloat = 0
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            keyBoardHeight = keyboardSize.height
            
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, animations: {
            
            self.bottomConstraint.constant = keyBoardHeight
            self.bottomView.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.2, delay: 0, animations: {
            
            self.bottomConstraint.constant = 0
            self.bottomView.isHidden = false
            self.view.layoutIfNeeded()
        })
    }
}
