//
//  MainTabBarViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 20/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class MainTabBarController : UITabBarController {
    
    // MARK: - Enumerations
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        delegate = self
    }
    
}


extension MainTabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if let navController = viewController as? UINavigationController {
            
            navController.popToRootViewController(animated: false)
        }
    }
}
