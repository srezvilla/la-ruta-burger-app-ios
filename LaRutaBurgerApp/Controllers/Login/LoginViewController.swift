//
//  LoginViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 15/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit
import FirebaseAuth


class LoginViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var txtEmailInput: UITextField!
    @IBOutlet weak var txtPasswordInput: UITextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnNewUser: UIButton!
    @IBOutlet weak var btnAnonymousUser: UIButton!
    @IBOutlet weak var lblMainTitle: UILabel!
    
    // MARK: - Constants
    
    let loginPassSegue = "loginSuccessSegue"
    
    
    // MARK: - Setup View
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        setupLabels()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    func setupLabels() {
        
        lblMainTitle.text =  localizable(.key_mainTitle)        
        btnForgotPassword.setTitle(localizable(.key_forgotPassword), for: .normal)
        btnLogin.setTitle(localizable(.key_login), for: .normal)
        btnNewUser.setTitle(localizable(.key_newUser), for: .normal)
        btnAnonymousUser.setTitle(localizable(.key_anonymousUser), for: .normal)
        txtEmailInput.placeholder = localizable(.key_email)
        txtPasswordInput.placeholder = localizable(.key_password)
    }
    
    func setupView() {
        
        dismissKeyboardWhenBackgroundTap()
        
        btnLogin.roundCorners()
        btnLogin.backgroundColor = CustomColor.tangerine
        
        btnNewUser.roundCorners()
        btnNewUser.backgroundColor = CustomColor.cerulean
        
        btnAnonymousUser.roundCorners()
        btnAnonymousUser.addBorderColor(CustomColor.blackAlpha10, borderWidth: 1.0)
        
        txtEmailInput.addBottomBorderWithColor(color: CustomColor.blackAlpha10, width: 1.0)
        txtEmailInput.setLeftPaddingPoints(10)
        txtEmailInput.textColor = CustomColor.blackAlpha80
        
        txtPasswordInput.addBottomBorderWithColor(color: CustomColor.blackAlpha10, width: 1.0)
        txtPasswordInput.setLeftPaddingPoints(10)
        txtPasswordInput.textColor = CustomColor.blackAlpha80
        
        let nextDataModelEmail = BarButtonDataModel(target: self, selector: #selector(nextButtonEmail), title: localizable(.key_next))
        let nextDataModelPassword = BarButtonDataModel(target: self, selector: #selector(nextButtonPassword), title: localizable(.key_done))
        
        txtEmailInput.addKeyboardTwoButtons(rightDataModel: nextDataModelEmail)
        txtPasswordInput.addKeyboardTwoButtons(rightDataModel: nextDataModelPassword)
    }
    
    @objc func nextButtonEmail(){
        
        txtPasswordInput.becomeFirstResponder()
    }
    
    @objc func nextButtonPassword(){
        
        dismissKeyboard()
    }
    
    
    // MARK: - Actions
    
    
    @IBAction func loginTapped(_ sender: UIButton) {

        showLoader()
        
        AppSession.logIn(txtEmailInput.text!, txtPasswordInput.text!, completion:{ error in
            
            self.hideLoader()
            
            if error == nil {
                
                self.performSegue(withIdentifier: self.loginPassSegue, sender: self)
                
            } else {
                
                self.showGenericError(error!)
            }
        })
    }
    
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        
        
        let alert = UIAlertController(title: localizable(.key_resetPasswordTitle),
                                        message: localizable(.key_resetPasswordText),
                                        preferredStyle: .alert)
        
         let saveAction = UIAlertAction(title: localizable(.key_send), style: .default) { action in
            
            self.showLoader()
            
            let emailField = alert.textFields![0]
            
            Auth.auth().sendPasswordReset(withEmail: emailField.text!) { error in
                
                self.hideLoader()
                
                if error == nil {
                    
                    let alertController = UIAlertController(title: localizable(.key_resetPasswordTitle
                    ), message: localizable(.key_resetPasswordConfirmation), preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: localizable(.key_ok), style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    let alertController = UIAlertController(title: localizable(.key_error
                    ), message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: localizable(.key_ok), style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }

        let cancelAction = UIAlertAction(title: localizable(.key_cancel),style: .default)
        alert.addTextField { textEmail in textEmail.placeholder = localizable(.key_email) }
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        
        present(alert, animated: true, completion: nil)
    }
    
}
