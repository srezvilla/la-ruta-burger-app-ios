//
//  AnonymousLoginViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class AnonymousLoginViewController: UIViewController {

    
    // MARK: - IBOutlets
    
    @IBOutlet weak var lblMainTitle: UILabel!
    @IBOutlet weak var lblMainText: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    // MARK: - Constants
    
    let loginPassSegue = "loginSegue"
    
    
    // MARK: - Setup View
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        setupLabels()
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    func setupLabels() {
        
        lblMainTitle.text = localizable(.key_anonymousUser)
        lblMainText.text = localizable(.key_anonymousUserText)
        btnLogin.setTitle(localizable(.key_login), for: .normal)
        btnCancel.setTitle(localizable(.key_cancel), for: .normal)
    }
    
    func setupView() {
                
        btnLogin.roundCorners()
        btnCancel.roundCorners()
        btnCancel.addBorderColor(CustomColor.blackAlpha10, borderWidth: 1.0)
    }
    
    // MARK: - Actions
    
    @IBAction func loginAction(_ sender: UIButton) {
        
        showLoader()
        
        AppSession.logIn(anonymousEmail, anonymousPassword, completion:{ error in
            
            self.hideLoader()
            
            if error == nil {
                
                AppSession.anonymousUser = true
                self.performSegue(withIdentifier: self.loginPassSegue, sender: self)
                
            } else {
                
                self.showGenericError(error!)
            }

        })
    }
    
    

}
