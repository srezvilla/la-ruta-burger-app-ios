//
//  BurgerDataModel.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 23/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import Foundation


struct BurgerDataModel {
    
    private struct SerializationKeys {
        
        static let name = "name"
        static let description = "description"
        static let likes = "likes"
        static let restaurant = "restaurant"
        static let restaurantName = "restaurantName"
        static let userName = "userName"
        static let reference = "reference"
        static let documentId = "documentId"
    }
    
    var name: String
    var description: String
    var likes: Int
    var restaurant: String
    var restaurantName: String
    var userName: String
    var reference: String
    var documentId: String
    
    init?(dictionary: [String: Any], reference: String, documentId: String) {
        
        guard let name = dictionary[SerializationKeys.name] as? String else { return nil }
        self.name = name
        
        guard let description = dictionary[SerializationKeys.description] as? String else { return nil }
        self.description = description
        
        guard let likes = dictionary[SerializationKeys.likes] as? Int else { return nil }
        self.likes = likes
        
        guard let restaurant = dictionary[SerializationKeys.restaurant] as? String else { return nil }
        self.restaurant = restaurant
        
        guard let restaurantName = dictionary[SerializationKeys.restaurantName] as? String else { return nil }
        self.restaurantName = restaurantName
        
        guard let userName = dictionary[SerializationKeys.userName] as? String else { return nil }
        self.userName = userName
        
        self.reference = reference
        self.documentId = documentId
    }
    
    init?(_ name: String, _ description: String , _ likes: Int, _ restaurant: String, _ restaurantName: String, _ userName: String, _ reference: String, _ documentId: String) {
        
        self.name = name
        self.description = description
        self.likes = likes
        self.restaurant = restaurant
        self.restaurantName = restaurantName
        self.userName = userName
        self.reference = reference
        self.documentId = documentId
    }
    
    func dictionaryRepresentation() -> [String : Any] {
        
        var dictionary: [String: Any] = [:]
        
        dictionary[SerializationKeys.name] = name
        dictionary[SerializationKeys.description] = description
        dictionary[SerializationKeys.likes] = likes
        dictionary[SerializationKeys.restaurant] = restaurant
        dictionary[SerializationKeys.restaurantName] = restaurantName
        dictionary[SerializationKeys.userName] = userName
        dictionary[SerializationKeys.reference] = reference
        dictionary[SerializationKeys.documentId] = documentId
        
        return dictionary
    }
    
}
