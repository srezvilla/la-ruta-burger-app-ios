//
//  ReviewDataModel.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 22/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import Foundation


struct ReviewDataModel {
    
    private struct SerializationKeys {
        
        static let userName = "userName"
        static let title = "title"
        static let description = "description"
        static let ranking = "ranking"
        static let reference = "reference"
    }
    
    let userName: String
    let title: String
    let description: String?
    let ranking: Int
    let reference: String
    
    
    init?(dictionary: [String: Any], reference: String) {
        
        guard let userName = dictionary[SerializationKeys.userName] as? String else { return nil }
        self.userName = userName
        
        guard let title = dictionary[SerializationKeys.title] as? String else { return nil }
        self.title = title
        
        self.description = dictionary[SerializationKeys.description] as? String
        
        let rankingNumber = dictionary[SerializationKeys.ranking] as? String
        
        if rankingNumber == nil {
            
            self.ranking = 0
            
        } else {
            
            let rankingTranslation: Int? = Int(rankingNumber!)
            
            if rankingTranslation != nil && rankingTranslation! > -1 && rankingTranslation! < 11{
                
                self.ranking = rankingTranslation!
                
            } else {
                
                self.ranking = 0
            }
        }
        
        self.reference = reference
    }
    
    init?(_ userName:String, _ title: String, _ description: String? , _ ranking: Int, _ reference: String) {
        
        self.userName = userName
        self.title = title
        self.description = description
        self.ranking = ranking
        self.reference = reference
    }
    
    
    func dictionaryRepresentation() -> [String : Any] {
        
        var dictionary: [String: Any] = [:]
        
        dictionary[SerializationKeys.userName] = userName
        dictionary[SerializationKeys.title] = title
        dictionary[SerializationKeys.description] = description != nil ? description : ""
        dictionary[SerializationKeys.ranking] = String(ranking)
        dictionary[SerializationKeys.reference] = reference
        
        return dictionary
    }
    
}

