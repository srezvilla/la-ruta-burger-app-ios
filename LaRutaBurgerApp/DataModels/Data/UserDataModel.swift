//
//  UserDataModel.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 20/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import Foundation


struct UserData {
    
    private struct SerializationKeys {
        
        static let fullName = "fullName"
        static let email = "email"
        static let likes = "likes"
        static let reference = "reference"
    }
    
    var fullName: String
    var email: String
    var likes: [String]
    var reference: String

    
    init?(dictionary: [String: Any], reference: String) {
        
        guard let fullName = dictionary[SerializationKeys.fullName] as? String else { return nil }
        self.fullName = fullName
        
        guard let email = dictionary[SerializationKeys.email] as? String else { return nil }
        self.email = email
        
        guard let likes = dictionary[SerializationKeys.likes] as? [String] else { return nil }
        self.likes = likes
        
        self.reference = reference
    }
    
    init?(_ name: String, _ email: String , _ likes: [String], _ reference: String) {
        
        self.fullName = name
        self.email = email
        self.likes = likes
        self.reference = reference
    }
    
    func getFullName() -> String {
        
        return fullName
    }
    
    func getEmail() -> String {
        
        return email
    }
    
    func dictionaryRepresentation() -> [String : Any] {
        
        var dictionary: [String: Any] = [:]
        
        dictionary[SerializationKeys.fullName] = fullName
        dictionary[SerializationKeys.email] = email
        dictionary[SerializationKeys.likes] = likes
        dictionary[SerializationKeys.reference] = reference
        
        return dictionary
    }
    
}
