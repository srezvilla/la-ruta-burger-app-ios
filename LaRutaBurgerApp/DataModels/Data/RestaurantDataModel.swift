//
//  RestaurantDataModel.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import Foundation


struct RestaurantData {
    
    private struct SerializationKeys {
        
        static let name = "name"
        static let address = "address"
        static let latitud = "latitud"
        static let longitud = "longitud"
        static let description = "description"
        static let web = "web"
        static let share = "share"
        static let ranking = "ranking"
        static let reference = "reference"
        static let documentId = "documentId"
    }
    
    let name: String
    let address: String
    let latitud: Double
    let longitud: Double
    let description: String?
    let web: String?
    let share: String?
    let ranking: Int
    let reference: String
    let documentId: String
    
    
    init?(dictionary: [String: Any], reference: String, documentId: String) {
        
        guard let name = dictionary[SerializationKeys.name] as? String else { return nil }
        self.name = name
        
        guard let address = dictionary[SerializationKeys.address] as? String else { return nil }
        self.address = address
        
        guard let latitud = dictionary[SerializationKeys.latitud] as? String else { return nil }
        if let latitudConverted = Double(latitud) { self.latitud = latitudConverted } else { return nil }
        
        guard let longitud = dictionary[SerializationKeys.longitud] as? String else { return nil }
        if let longitudConverted = Double(longitud) { self.longitud = longitudConverted } else { return nil }
        
        self.description = dictionary[SerializationKeys.description] as? String
        
        self.web = dictionary[SerializationKeys.web] as? String
        
        self.share = dictionary[SerializationKeys.share] as? String
        
        let rankingNumber = dictionary[SerializationKeys.ranking] as? String
        
        if rankingNumber == nil {
            
            self.ranking = 0
            
        } else {
            
            let rankingTranslation: Int? = Int(rankingNumber!)
            
            if rankingTranslation != nil && rankingTranslation! > -1 && rankingTranslation! < 11{
                
                self.ranking = rankingTranslation!
                
            } else {
                
                self.ranking = 0
            }
        }
        
        self.reference = reference
        self.documentId = documentId
    }
    
    init?(_ name: String, _ address: String, _ latitud: Double, _ longitud: Double, _ description: String? ,_ web: String?, _ share: String?, _ ranking: Int, _ reference: String, _ documentId: String) {
        
        self.name = name
        self.address = address
        self.latitud = latitud
        self.longitud = longitud
        self.description = description
        self.web = web
        self.share = share
        self.ranking = ranking
        self.reference = reference
        self.documentId = documentId
    }
    
    
    func dictionaryRepresentation() -> [String : Any] {
        
        var dictionary: [String: Any] = [:]        
        
        dictionary[SerializationKeys.name] = name
        dictionary[SerializationKeys.address] = address
        dictionary[SerializationKeys.latitud] = String(latitud)
        dictionary[SerializationKeys.longitud] = String(longitud)
        dictionary[SerializationKeys.description] = description != nil ? description : ""
        dictionary[SerializationKeys.web] = web != nil ? web : ""
        dictionary[SerializationKeys.share] = share != nil ? share : ""
        dictionary[SerializationKeys.ranking] = String(ranking)
        dictionary[SerializationKeys.reference] = reference
        dictionary[SerializationKeys.documentId] = documentId
        
        
        return dictionary
    }
    
    func dictionaryRepresentation(_ newRanking: Int) -> [String : Any] {
        
        var dictionary: [String: Any] = [:]
        
        dictionary[SerializationKeys.name] = name
        dictionary[SerializationKeys.address] = address
        dictionary[SerializationKeys.latitud] = String(latitud)
        dictionary[SerializationKeys.longitud] = String(longitud)
        dictionary[SerializationKeys.description] = description != nil ? description : ""
        dictionary[SerializationKeys.web] = web != nil ? web : ""
        dictionary[SerializationKeys.share] = share != nil ? share : ""
        dictionary[SerializationKeys.ranking] = String(newRanking)
        dictionary[SerializationKeys.reference] = reference
        dictionary[SerializationKeys.documentId] = documentId
        
        return dictionary
    }
    
}
