//
//  BarButtonDataModel.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 18/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

struct BarButtonDataModel {
    
    var target: Any
    var selector: Selector
    var title: String?
    var style: UIBarButtonItemStyle?
    var image: UIImage?
    
    init(target: Any,
         selector: Selector,
         title: String?,
         style: UIBarButtonItemStyle? = .plain) {
        
        self.target = target
        self.selector = selector
        self.title = title
        self.style = style
    }
}
