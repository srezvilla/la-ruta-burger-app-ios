//
//  BarItemModel.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import Foundation
import UIKit

struct BarItemDataModel {
    
    var title: String?
    var icon: UIImage?
    var targetDataModel: TargetModel!
    
    init(title: String? = nil,
         icon: UIImage? = nil,
         targetDataModel: TargetModel) {
        
        self.title = title
        self.icon = icon
        self.targetDataModel = targetDataModel
    }
}
