//
//  TargetModel.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 21/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

struct TargetModel {
    
    var target: Any
    var selector: Selector
    var controlEvent: UIControlEvents
    var identifier: String?
    
    init(target: Any,
         selector: Selector,
         controlEvent: UIControlEvents? = .touchUpInside,
         identifier: String? = nil) {
        
        self.target = target
        self.selector = selector
        self.controlEvent = controlEvent!
        self.identifier = identifier
    }
}

