//
//  PhotoLibraryManager.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 25/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import Photos

class PhotoLibraryManager {
    
    // MARK - Public
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        
        let authorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch authorizationStatus {
            
        case .authorized:
            completionHandler(true)
            
        case .denied:
            showSettingsAlert(completionHandler)
            
        case .restricted, .notDetermined:
            
            PHPhotoLibrary.requestAuthorization({ (status) in
                
                if status == .authorized {
                    
                    completionHandler(true)
                    
                } else {
                    
                    DispatchQueue.main.async {
                        
                        self.showSettingsAlert(completionHandler)
                    }
                }
            })
        }
    }
    
    func imagePickerDidFinishPickingMediaWithInfo(_ info: [String : Any], completion: @escaping (UIImage?,Data?) -> ()) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            var data = Data()
            
            data = UIImageJPEGRepresentation(image, 0.8)!
            
            completion(image,data)
            
        } else {
            
            completion(nil,nil)
        }
    }
    
    // MARK - Private
    
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        
    }
}
