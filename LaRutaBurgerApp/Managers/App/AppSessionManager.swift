//
//  AppSessionManager.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 17/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit
import MapKit
import FirebaseAuth


class AppSessionManager: NSObject {
    
    // MARK: - Singleton
    
    static let sharedInstance = AppSessionManager()
    
    private override init() {
        
        super.init()
    }
    
    
    // MARK: - Session enumeration keys
    
    enum SessionConstant: String  {
        
        case appPreferredLanguage = "appPreferredLanguage"
    }
    
    
    //MARK: - Session properties
    
    var userData: UserData?
    var userCordinate: CLLocation = defaultUserLocation
    var restaurantLoacation: CLLocation?
    var anonymousUser: Bool = false
    
    
    // MARK: Persisted in UserDefaults
    
    
    var appPreferredLanguage: AppLanguage {
        set {
            setValue(newValue.rawValue, constant: .appPreferredLanguage)
        }
        
        get {
            var languageString = getString(.appPreferredLanguage)
            
            if languageString == nil {
                
                var deviceLanguage = Locale.preferredLanguages[0].uppercased()
                
                if deviceLanguage.count > 2 {
                    
                    deviceLanguage = deviceLanguage.substring(toIndex: 2)!
                }
                
                languageString = AppLanguage.rawValues().contains(deviceLanguage) ? deviceLanguage : AppConstant.defaultLanguage.rawValue
            }
            
            return AppLanguage(rawValue: languageString!)!
        }
    }
    
    func setValue(_ value: Any?, constant: SessionConstant) {
        
        if value != nil {
            
            UserDefaults.standard.set(value, forKey: constant.rawValue)
            
        } else {
            
            UserDefaults.standard.removeObject(forKey: constant.rawValue)
        }
    }
    
    
    // MARK: String
    
    func getString(_ constant: SessionConstant) -> String? {
        
        return getValue(constant, returnType: String.self)
    }
    
    func getValue<T>(_ constant: SessionConstant, returnType: T.Type) -> T? {
        
        return UserDefaults.standard.value(forKey: constant.rawValue) as? T
    }
     
    func logout() {
        
        do {
            try Auth.auth().signOut()
            
            cleanProperties()
            let storyBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login")
            appDelegate.window?.rootViewController = storyBoard
            appDelegate.window?.isHidden = false
            
        } catch let signOutError as NSError {
            
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func cleanProperties() {
        
        userData = nil
        restaurantLoacation = nil
        anonymousUser = false
    }
    
    // MARK: - Login
    
    func logIn(_ email: String, _ password: String, completion: @escaping (String?) -> ()) {
        
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            
            if error == nil {
                
                dbManager.getUser(email) { (userData,error) in
                    
                    if error == nil && userData != nil {
                        
                        self.userData = userData
                        completion(nil)
                        
                    } else {
                        
                        completion(error)
                    }
                }
                
            } else {
                
                completion(error!.localizedDescription)
                
            }

        }
    }
    
    // MARK: - New user
    
    func newUser(_ email: String, _ fullName: String, _ password: String, completion: @escaping (String?) -> ()) {
        
            Auth.auth().createUser(withEmail: email,
                                   password: password) { user, error in
                                    
                                    if error == nil {
                                        
                                        completion(nil)
                                        
                                    } else {
                                        
                                        completion(error!.localizedDescription)
                                    }
            }
    }
        
    
    // MARK: - Burgers Like
    
    public func hasBurgerLike(reference: String) -> Bool {
        
        if userData != nil {
            
            for like in userData!.likes {
                
                if like == reference {
                    
                    return true
                }
            }
        }
        
        return false
    }
    
    public func userLikeBurger(reference: String, like: Bool, completion: @escaping (String?) -> ()) {
        
        if userData != nil {
            
            if like {
                
                if let _ = userData!.likes.index(of: reference) {
                    
                    return
                    
                } else {
                    
                    userData!.likes.append(reference)
                }
                
            } else {
                
                if let index = userData!.likes.index(of: reference) {
                    
                    userData!.likes.remove(at: index)
                }
            }
            
            dbManager.updateUser(userData: self.userData!, reference: userData!.reference) { error in
                
                if error == nil {
                    
                    completion(nil)
                    
                } else {
                    
                    completion(error!)
                }
                
            }
        }
    }
    

    
}
