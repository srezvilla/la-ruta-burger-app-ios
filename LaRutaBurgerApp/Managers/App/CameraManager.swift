//
//  CameraManager.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 25/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import AVFoundation
import UIKit

class CameraManager {
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        
        switch authorizationStatus {
            
        case .authorized:
            completionHandler(isCameraAvailable())
            
        case .denied:
            showSettingsAlert(completionHandler)
            
        case .restricted, .notDetermined:
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                
                if granted {
                    
                    completionHandler(self.isCameraAvailable())
                    
                } else {
                    
                    DispatchQueue.main.async {
                        
                        self.showSettingsAlert(completionHandler)
                    }
                }
            })
        }
    }
    
    private func isCameraAvailable() -> Bool {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            return true
            
        } else {
            
            DispatchQueue.main.async {
                

            }
            
            return false
        }
    }
    
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        

    }
}
