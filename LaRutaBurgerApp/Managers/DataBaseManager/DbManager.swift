//
//  DbManager.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 29/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import Firebase
import FirebaseFirestore

class DbManager: NSObject {
    
    
    // MARK: - Singleton
    
    static let sharedInstance = DbManager()
    
    private override init() {
        
        super.init()
    }
    
    
    // MARK: - Setup Manager
    
    let dbFirestore = Firestore.firestore()
    
    
    // MARK: - Get User
    
    func getUser(_ email: String, completion: @escaping (UserData?,String?) -> ()) {
        
        
        let usersRef = dbManager.dbFirestore.collection(collections.users.rawValue)
        
        usersRef.whereField("email", isEqualTo: email).getDocuments() { (querySnapshot, error) in
            
            if let error = error {
                
                completion(nil,error.localizedDescription)
                
            } else {
                
                for document in querySnapshot!.documents {
                    
                    let ref = document.reference.path

                    let userData = UserData(dictionary: document.data(), reference: ref)
                    
                    completion(userData, nil)
                }
            }
        }
    }
    
    
    func getUserReference(_ email: String, completion: @escaping (String?,String?) -> ()) {
                
        let usersRef = dbManager.dbFirestore.collection(collections.users.rawValue)
        
        usersRef.whereField("email", isEqualTo: email).getDocuments() { (querySnapshot, error) in
            
            if let error = error {
                
                completion(nil,error.localizedDescription)
                
            } else {
                for document in querySnapshot!.documents {
                    
                    completion(document.reference.path, nil)
                }
            }
        }
    }
    
    // MARK: - Update User
    
    func updateUser(userData: UserData, reference: String, completion: @escaping (String?) -> ()) {
        
        let ref = dbManager.dbFirestore.document(reference)
            
        ref.setData(userData.dictionaryRepresentation()) { error in
                
            if error == nil {
                    
                completion(nil)
                    
            } else {
                    
                completion(error!.localizedDescription)
            }
        }
    }
    
    
    // MARK: - Add User
    
    func addUser(userData: UserData, completion: @escaping (String?,String?) -> ()) {
        
        var ref: DocumentReference? = nil
        
        ref = dbManager.dbFirestore.collection(collections.users.rawValue).addDocument(data: userData.dictionaryRepresentation()) { error in
                        
            if error == nil {
                
                completion(ref?.path,nil)
             
            } else {
                
                completion(nil,error!.localizedDescription)
            }
        }
    }
    
    
    // MARK: - Get Restaurants
    
    func getRestaurants(completion: @escaping ([RestaurantData],String?) -> ()) {
        
        dbManager.dbFirestore.collection(collections.restaurants.rawValue).getDocuments { (querySnapshot, error) in
            
            if let error = error {

                completion([],error.localizedDescription)
                
            } else {
                
                var restaurants: [RestaurantData] = []
                
                for document in querySnapshot!.documents {
                    
                    let reference = document.reference
                    
                    if let restaurantData = RestaurantData.init(dictionary: document.data(), reference: reference.path, documentId: document.documentID) {
                        
                        restaurants.append(restaurantData)
                    }
                }
                
                completion(restaurants,nil)
            }
        }
    }
    
    // MARK: - Ger Restaurant
    
    func getRestaurant(reference: String, completion: @escaping (RestaurantData?,String?) -> ()) {
        
        let document =  dbFirestore.document(reference)
        
        document.getDocument() { (data,error) in
            
            if error == nil {
                
                let data = data?.data()
                
                let restaurandData = RestaurantData.init(dictionary: data!, reference: reference, documentId: document.documentID)
                    
                completion(restaurandData ,nil)                                                
                
            } else {
                
                completion(nil, error!.localizedDescription)
            }
        }                
    }
    
    
    // MARK: - Get Restaurant Reviews
    
    func getRestaurantReviews(reference: String, completion: @escaping ([ReviewDataModel],String?) -> ()) {
        
        dbFirestore.collection(collections.reviews.rawValue).whereField("reference", isEqualTo: reference).getDocuments() { (querySnapshot, error) in
            
            if let error = error {
                
                completion([],error.localizedDescription)
                
            } else {
                
                var reviews: [ReviewDataModel] = []
                
                for document in querySnapshot!.documents {
                    
                    let reference = document.reference
                    
                    if let reviewData = ReviewDataModel.init(dictionary: document.data(), reference: reference.path) {
                        
                        reviews.append(reviewData)
                    }
                }
                
                completion(reviews,nil)
            }
        }
    }
    
    // MARK: - Add Review
    
    func addReview(reviewData: ReviewDataModel, completion: @escaping (String?) -> ()) {
        
        dbManager.dbFirestore.collection(collections.reviews.rawValue).addDocument(data: reviewData.dictionaryRepresentation()) { error in
            
            if error == nil {
                
                completion(nil)
                
            } else {
                
                completion(error!.localizedDescription)
            }
        }
    }
    
    
    // MARK: - Update Restaurat
    
    func updateRestaurant(reference: String, restaurantData: [String: Any], completion: @escaping (String?) -> ()) {
        
        let restaurantRef = dbManager.dbFirestore.document(reference)

        restaurantRef.setData(restaurantData) { error in
            
            if error == nil {
                
                completion(nil)
                
            } else {
                
                completion(error!.localizedDescription)
            }
        }
    }
    
    // MARK: - Get Burgers
    
    func getBurgers(completion: @escaping ([BurgerDataModel],String?) -> ()) {
        
        dbManager.dbFirestore.collection(collections.burgers.rawValue).getDocuments { (querySnapshot, error) in
            
            if let error = error {
                
                completion([],error.localizedDescription)
                
            } else {
                
                var burgers: [BurgerDataModel] = []
                
                for document in querySnapshot!.documents {
                    
                    if let burgerData = BurgerDataModel.init(dictionary: document.data(), reference: document.reference.path, documentId: document.documentID) {
                        
                        burgers.append(burgerData)
                    }
                }
                
                completion(burgers,nil)
            }
        }
    }
    
    // MARK: - Update Burger
    
    func updateBurger(reference: String, burgerData: [String: Any], completion: @escaping (String?) -> ()) {
        
        let ref = dbManager.dbFirestore.document(reference)
        
        ref.setData(burgerData) { error in
            
            if error == nil {
                
                completion(nil)
                
            } else {
                
                completion(error!.localizedDescription)
            }
        }
    }
    
    // MARK: - Add Burger
    
    func addBurger(reviewData: BurgerDataModel, completion: @escaping (String?,String?) -> ()) {
        
        var ref: DocumentReference? = nil
        
        ref = dbManager.dbFirestore.collection(collections.burgers.rawValue).addDocument(data: reviewData.dictionaryRepresentation()) { error in
            
            if error == nil {
                
                completion(nil,ref?.documentID)
                
            } else {
                
                completion(error!.localizedDescription,nil)
            }
        }
    }
    

}


