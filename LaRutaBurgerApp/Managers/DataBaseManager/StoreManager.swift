//
//  StoreManager.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 5/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import Firebase
import FirebaseStorage

class StorageManager: NSObject {
    
    // MARK: - Singleton
    
    static let sharedInstance = StorageManager()
    
    private override init() {
        
        super.init()
    }
    
    // MARK: - Setup Manager
    
    let storage = Storage.storage()
    
    
    // MARK: - Add Burger Photo
    
    func addBurgerPhoto(data: Data, ref: String, completion: @escaping (String?) -> ()) {
    
        let burgerRef = StorageReference.init().child("burgers/" + ref + ".jpg")
        
        burgerRef.putData(data, metadata: nil) { (metadata, error) in

            if error == nil {
                
                completion(nil)
                
            } else {
                
                completion(error?.localizedDescription)
            }
        }

    }
    
    
    // MARK: - Get Burger Photo
    
    func getBurgerPhoto(ref: String, completion: @escaping (Data?,String?) -> ()) {
        
        let burgerRef = StorageReference.init().child("burgers/" + ref + ".jpg")
        
        burgerRef.getData(maxSize: 10*1024*1024) { (data,error) in
            
            if error == nil {
                
                completion(data!,nil)
                
            } else {
                
                completion(nil, error!.localizedDescription)
            }
        }
    }

    // MARK: - Get Restaurant Photo
    
    func getRestaurantPhoto(ref: String, completion: @escaping (Data?,String?) -> ()) {
        
        let burgerRef = StorageReference.init().child("restaurants/" + ref + ".jpg")
        
        burgerRef.getData(maxSize: 10*1024*1024) { (data,error) in
            
            if error == nil {
                
                completion(data!,nil)
                
            } else {
                
                completion(nil, error!.localizedDescription)
            }
        }
    }
    
}
