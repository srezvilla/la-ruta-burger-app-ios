//
//  MenuViewController.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 19/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit
import FirebaseAuth

class MenuViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var btnLogOut: UIButton!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var lblNameValue: UILabel!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var lblEmailValue: UILabel!
    @IBOutlet weak var btnChangePass: UIButton!
    @IBOutlet weak var lblLanguageTitle: UILabel!
    @IBOutlet weak var sgmLanguage: UISegmentedControl!
    
    // MARK: - Properties
    
    var storyBoardOrigin: StoryBoard = .ranking
    var userData: UserData?
    
    // MARK: - Override functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupNavigationBar()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        setupLabels()
        hideTabBar()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        showTabBar()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Setup
    
    private func setupNavigationBar() {
        
        addsLeftBarButton(image: CustomImage.NavBar.back,targetsDataModel: [TargetModel(target: self, selector: #selector(backAction))])
        
        navigationController?.removeShadow()
        extendedLayoutIncludesOpaqueBars = true
    }
    
    private func setupLabels() {
        
        navigationItem.title = localizable(.key_mainTitle)
        btnLogOut.setTitle(localizable(.key_logOut), for: .normal)
        
        lblNameTitle.textColor = CustomColor.blackAlpha50
        lblNameTitle.text = localizable(.key_fullName)
        
        lblNameValue.textColor = CustomColor.blackAlpha50
        lblNameValue.text = userData != nil ? userData?.getFullName() : localizable(.key_notAvailable)
        
        lblEmailTitle.textColor = CustomColor.blackAlpha50
        lblEmailTitle.text = localizable(.key_email)
        
        lblEmailValue.textColor = CustomColor.blackAlpha50
        
        
        lblEmailValue.text = userData != nil ? userData?.getEmail() : localizable(.key_notAvailable)
        
        lblLanguageTitle.textColor = CustomColor.blackAlpha50
        lblLanguageTitle.text = localizable(.key_language)
        
        if AppSession.anonymousUser {
            
            lblNameValue.text = localizable(.key_notAvailable)
            lblEmailValue.text = localizable(.key_notAvailable)
            btnChangePass.isEnabled = false
        }
    }
    
    private func setupView() {
        
        userData = AppSession.userData
        
        hidesBottomBarWhenPushed = true
        btnLogOut.roundCorners()
        btnLogOut.addBorderColor(CustomColor.blackAlpha10, borderWidth: 1.0)
        btnChangePass.adjustsImageWhenHighlighted = false
        
        if AppSession.appPreferredLanguage == AppLanguage.es {
            
            sgmLanguage.selectedSegmentIndex = 0
            
        } else {
            
            sgmLanguage.selectedSegmentIndex = 1
        }
    }

    private func changeLanguage(language: AppLanguage) {
        
        AppSession.setValue(language.rawValue, constant: .appPreferredLanguage)
        setupLabels()
    }
    
    // MARK: - Navigation
    
    @objc func backAction() {
        
        popViewController()
    }
    
    // MARK: - Actions
    
    @IBAction func logOutAction(_ sender: UIButton) {
        
        AppSession.logout()
    }
    
    @IBAction func btnSpanishAction(_ sender: Any) {
        
        changeLanguage(language: AppLanguage.es)
    }
    
    
    @IBAction func btnEnglishAction(_ sender: UIButton) {
        
        changeLanguage(language: AppLanguage.en)
    }
    
    @IBAction func sgmLanguageAction(_ sender: Any) {
        
        if sgmLanguage.selectedSegmentIndex == 0 {
            
            changeLanguage(language: AppLanguage.es)
            
        } else {
            
            changeLanguage(language: AppLanguage.en)
        }
    }

    @IBAction func btnChangePassAction(_ sender: UIButton) {
        
        let alert = UIAlertController(title: localizable(.key_resetPasswordTitle),
                                      message: localizable(.key_resetPasswordText),
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: localizable(.key_send), style: .default) { action in
            
            self.showLoader()
            
            
            Auth.auth().sendPasswordReset(withEmail: self.userData != nil ? self.userData!.getEmail() : localizable(.key_notAvailable)) { error in
                
                self.hideLoader()
                
                if error == nil {
                    
                    let alertController = UIAlertController(title: localizable(.key_resetPasswordTitle
                    ), message: localizable(.key_resetPasswordConfirmation), preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: localizable(.key_ok), style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    let alertController = UIAlertController(title: localizable(.key_error
                    ), message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: localizable(.key_ok), style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: localizable(.key_cancel),style: .default)
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        
        present(alert, animated: true, completion: nil)
    }
}
