//
//  LoaderUIView.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 16/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class LoaderUIView: UIView {
    
    // MARK: - IBOutlets

    @IBOutlet var mainView: LoaderUIView!
    @IBOutlet weak var imgLoader: UIImageView!
    
    // MARK: Setup
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        //setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        //setup()
    }
    
    func setup() {
        
        Bundle.main.loadNibNamed("LoaderUIView", owner: self, options: nil)
        addSubview(mainView)
        mainView.frame = self.bounds
        mainView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    
}
