//
//  BurgerRankTableViewCell.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 25/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class BurgerRankTableViewCell: UITableViewCell {

    
    // MARK: - IBOutlets
    
    @IBOutlet weak var imgAward: UIImageView!
    @IBOutlet weak var lblTItle: UILabel!
    @IBOutlet weak var lblRestaurant: UILabel!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var lblLikes: UILabel!
    
    // MARK: - Override functions
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Setup cell
    
    public func setupCell(burgerData: BurgerDataModel, index: Int) {
        
        let hasLike = AppSession.hasBurgerLike(reference: burgerData.reference)
        
        selectionStyle = .none
        lblTItle.text = burgerData.name
        lblRestaurant.text = burgerData.restaurantName
        lblUser.text = burgerData.userName
        lblLikes.text = String(burgerData.likes)
        imgLike.image = hasLike ? CustomImage.Icon.like : CustomImage.Icon.likeEmpty
        selectionStyle = .none
        
        switch index {
        case 0:
            imgAward.image = CustomImage.Img.goldAward
        case 1:
            imgAward.image = CustomImage.Img.silverAward
        case 2:
            imgAward.image = CustomImage.Img.bronzeAward
        default:
            imgAward.image = nil
        }
    }
    
}
