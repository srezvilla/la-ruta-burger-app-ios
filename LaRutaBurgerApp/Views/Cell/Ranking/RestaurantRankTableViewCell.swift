//
//  RestaurantRankTableViewCell.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 25/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class RestaurantRankTableViewCell: UITableViewCell {

    
    // MARK: - IBOutlets
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgAward: UIImageView!
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
        
    
    // MARK: - Override functions
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    private func setupStars(ranking: Int) {
        
        setupStar(starNumber: 1, ranking: ranking, starImage: imgStar1)
        setupStar(starNumber: 2, ranking: ranking, starImage: imgStar2)
        setupStar(starNumber: 3, ranking: ranking, starImage: imgStar3)
        setupStar(starNumber: 4, ranking: ranking, starImage: imgStar4)
        setupStar(starNumber: 5, ranking: ranking, starImage: imgStar5)
        
    }
    
    private func setupStar(starNumber: Int, ranking: Int, starImage: UIImageView){
        
        let rankingTranslate = ranking / 2
        
        if rankingTranslate < starNumber {
            
            if ranking % 2 == 0 {
                
                starImage.image = CustomImage.Img.emptyStar
                
            } else {
                
                let difference = starNumber - rankingTranslate
                
                if difference <= 1 {
                    
                    starImage.image = CustomImage.Img.halfStar
                    
                } else {
                    
                    starImage.image = CustomImage.Img.emptyStar
                }
            }
            
        } else {
            
            starImage.image = CustomImage.Img.star
        }
    }
    
    // MARK: - Public functions
    
    public func setupCell(restaurantData: RestaurantData, index: Int){

        lblName.text = restaurantData.name
        lblAddress.text = restaurantData.address
        setupStars(ranking: restaurantData.ranking)
        selectionStyle = .none
        
        switch index {
        case 0:
            imgAward.image = CustomImage.Img.goldAward
        case 1:
            imgAward.image = CustomImage.Img.silverAward
        case 2:
            imgAward.image = CustomImage.Img.bronzeAward
        default:
            imgAward.image = nil
        }
    }
    
}
