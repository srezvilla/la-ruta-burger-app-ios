//
//  MainContentTableViewCell.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 22/7/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class MainContentTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    
    @IBOutlet weak var burgerImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    // MARK: - Override Methods
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Setup View
    
    private func setupView() {
        
        mainView.layer.masksToBounds = false
        mainView.layer.shadowColor = CustomColor.boxShadow45.cgColor
        mainView.layer.shadowOpacity = 0.8
        mainView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        mainView.layer.shadowRadius = 2
    }
    

}
