//
//  RestaurantPickTableViewCell.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 25/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class RestaurantPickTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    
    // MARK: - Override functions

    override func awakeFromNib() {
        
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Setup cell
    
    public func setupCell(restaurantData: RestaurantData, _ selected: Bool) {
        
        lblName.text = restaurantData.name
        lblAddress.text = restaurantData.address
        
        selectionStyle = .none
        
        if selected {
            
            backgroundColor = CustomColor.tangerineAlpha
            
        } else {
            
            backgroundColor = CustomColor.white
        }
    }
    
}
