//
//  BurgerTableViewCell.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 23/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class BurgerTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbluserName: UILabel!
    @IBOutlet weak var lblRestaurant: UILabel!
    @IBOutlet weak var lblLikes: UILabel!
    @IBOutlet weak var imgLikes: UIImageView!
    
    
    // MARK: - Override functions

    override func awakeFromNib() {
        
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Setup cell
    
    public func setupCell(burgerData: BurgerDataModel) {
        
        let hasLike = AppSession.hasBurgerLike(reference: burgerData.reference)
        
        selectionStyle = .none
        lblTitle.text = burgerData.name
        lblRestaurant.text = burgerData.restaurantName
        lbluserName.text = burgerData.userName
        lblLikes.text = String(burgerData.likes)
        imgLikes.image = hasLike ? CustomImage.Icon.like : CustomImage.Icon.likeEmpty
    }
    
}
