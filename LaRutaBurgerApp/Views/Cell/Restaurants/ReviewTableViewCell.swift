//
//  ReviewTableViewCell.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 22/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTItle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    
    
    // MARK: - Override functions
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    
    // MARK: - Setup
    
    public func setupCell(reviewData: ReviewDataModel) {
        
        lblUserName.text = reviewData.userName
        lblTItle.text = reviewData.title
        lblDescription.text = reviewData.description
        lblUserName.textColor = CustomColor.blackAlpha80
        lblTItle.textColor = CustomColor.blackAlpha80
        lblDescription.textColor = CustomColor.blackAlpha80        
        setupStars(ranking: reviewData.ranking)
    }
    
    
    private func setupStars(ranking: Int) {
        
        setupStar(starNumber: 1, ranking: ranking, starImage: imgStar1)
        setupStar(starNumber: 2, ranking: ranking, starImage: imgStar2)
        setupStar(starNumber: 3, ranking: ranking, starImage: imgStar3)
        setupStar(starNumber: 4, ranking: ranking, starImage: imgStar4)
        setupStar(starNumber: 5, ranking: ranking, starImage: imgStar5)
        
    }
    
    private func setupStar(starNumber: Int, ranking: Int, starImage: UIImageView){
        
        let rankingTranslate = ranking / 2
        
        if rankingTranslate < starNumber {
            
            if ranking % 2 == 0 {
                
                starImage.image = CustomImage.Img.emptyStar
                
            } else {
                
                let difference = starNumber - rankingTranslate
                
                if difference <= 1 {
                    
                    starImage.image = CustomImage.Img.halfStar
                    
                } else {
                    
                    starImage.image = CustomImage.Img.emptyStar
                }
            }
            
        } else {
            
            starImage.image = CustomImage.Img.star
        }
    }
    
}
