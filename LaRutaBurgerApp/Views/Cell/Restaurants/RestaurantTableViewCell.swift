//
//  RestaurantTableViewCell.swift
//  LaRutaBurgerApp
//
//  Created by Sergio Alvarez on 22/8/18.
//  Copyright © 2018 Sergio Alvarez. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var viewSeparatorLine: UIView!
    
    // MARK: - Properties
    
    var restaurantData: RestaurantData?
    

    // MARK: - Override functions
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Setup cell
    
    private func setupView() {
        
        lblTitle.textColor = CustomColor.blackAlpha80
        lblTitle.font = CustomFont.semibold14
        lblAddress.textColor = CustomColor.blackAlpha80
        lblAddress.font = CustomFont.regular12
        viewSeparatorLine.backgroundColor = CustomColor.blackAlpha10
        self.selectionStyle = .none
    }
    
    private func setupStars(ranking: Int) {
        
        setupStar(starNumber: 1, ranking: ranking, starImage: imgStar1)
        setupStar(starNumber: 2, ranking: ranking, starImage: imgStar2)
        setupStar(starNumber: 3, ranking: ranking, starImage: imgStar3)
        setupStar(starNumber: 4, ranking: ranking, starImage: imgStar4)
        setupStar(starNumber: 5, ranking: ranking, starImage: imgStar5)
        
    }
    
    private func setupStar(starNumber: Int, ranking: Int, starImage: UIImageView){
        
        let rankingTranslate = ranking / 2
        
        if rankingTranslate < starNumber {
            
            if ranking % 2 == 0 {
                
                starImage.image = CustomImage.Img.emptyStar
                
            } else {
                
                let difference = starNumber - rankingTranslate
                
                if difference <= 1 {
                    
                    starImage.image = CustomImage.Img.halfStar
                    
                } else {
                    
                    starImage.image = CustomImage.Img.emptyStar
                }
            }
            
        } else {
            
            starImage.image = CustomImage.Img.star
        }
    }
    
    // MARK: - Public functions
    
    public func setupCell(restaurantData: RestaurantData){
        
        self.restaurantData = restaurantData
        
        lblTitle.text = restaurantData.name
        lblAddress.text = restaurantData.address
        setupStars(ranking: restaurantData.ranking)
    }
    
}
